﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tobii.XR;
using UnityEngine;

/// <summary>
/// Sensor system for the eye blink tracker.
/// Retrieves boolean values about left eye and right eye indicating the blink state.
/// Needs SR.Anipal and Tobii.XR
/// </summary>
public class EyeBlinkTracker : TrackingSystem
{
    /// <summary>
    /// Rest interval to calculate the current time delta
    /// </summary>
    private int restInterval;

    /// <summary>
    /// Storage for the blink status of the left eye (true -> closed, false -> open)
    /// </summary>
    private bool leftEyeBlinking;
    /// <summary>
    /// Storage for the blink status of the right eye (true -> closed, false -> open)
    /// </summary>
    private bool rightEyeBlinking;

    /// <summary>
    /// Storage for the current blink state to prevent duplicatee values.
    /// Only needed for "track every blink" (see <see cref="SendEveryBlink()"/>)
    /// </summary>
    private BlinkState blinkState;
    /// <summary>
    /// Storage for the current eye state to recognize blinking (state changes)
    /// Only needed for "track every blink" (see <see cref="SendEveryBlink()"/>)
    /// </summary>
    private EyeState eyeState;

    /// <summary>
    /// Instanciates a new eye blink tracker with the given values.
    /// Calls <see cref="TrackingSystem(string endpoint, int interval, OnFinsihAmount callback = null, int amountValues = -1, string settings = null)"/> with the given values.
    /// Needs to get an unique signature when implemented
    /// </summary>
    /// <param name="endpoint">Endpoint of the sensor on server side</param>
    /// <param name="interval">Interval in ms, in which the sensor updates its values</param>
    /// <param name="callback">Callback method to determine next state</param>
    /// <param name="amountValues">Amount of values, after which the state should change</param>
    /// <param name="settings">Settings which this sensor should overwrite on the server</param>
    public EyeBlinkTracker(string endpoint, int interval, OnFinsihAmount callback = null, int amountValues = -1, string settings = null)
        : base (endpoint, interval, callback, amountValues, settings)
    {
        Signature = typeof(EyeBlinkTracker).Name;
        if(interval == -1)// An interval of -1 indicates TrackEveryBlink!
        {
            restInterval = 0;
        }
        else
        {
            restInterval = interval;
        }

        if(AmountValues != -1 && AmountValues > 0)
        {
            AmountValues *= 2;
        }

        leftEyeBlinking = false;
        rightEyeBlinking = false;

        blinkState = BlinkState.NONE;
        eyeState = EyeState.UNDEFINED;
    }

    /// <summary>
    /// Implemented function from base class to overwrite mode. 
    /// Should normally recieve the result of the callback as param. 
    /// </summary>
    /// <param name="mode">New mode with amount of values</param>
    /// <returns>Success/Failure</returns>
    public override bool SetMode(TrackingSystemModeState mode)
    {
        Mode = mode.Mode;
        if (mode.AmountValues < 0 && mode.AmountValues != -1)
        {
            Debug.LogWarning(mode.AmountValues.ToString() + " is not a valid argument for tracking system " + Endpoint + "! Changing to -1. Adjust manually.");
            this.AmountValues = -1;
            return false;
        }
        AmountValues = mode.AmountValues;
        return true;
    }

    /// <summary>
    /// Updates the tracking system in the given interval.
    /// Returns true, if a new value is recorded.
    /// </summary>
    /// <param name="deltaTime">TimeDelta to the previous frame</param>
    /// <returns>True if new value, else false</returns>
    public override bool UpdateValue(float deltaTime)
    {
        var data = TobiiXR.GetEyeTrackingData(TobiiXR_TrackingSpace.Local);
        if(Interval == -1)
        {   
            return SendEveryBlink();
        }
        if (!CheckInterval(deltaTime))
        {
            return false;
        }
        if (!CheckAmount())
        {
            return false;
        }
        if (Mode != TrackingSystemMode.Idle)
        {
            CollectData(data);
            return true;
        }
        return false;
    }

    /// <summary>
    /// Alternative update methode for this tracking system.
    /// Checks via a state machine if the user currently blinks (see <see cref="DetectBlink(EyeState old, EyeState current)"/>).
    /// Returns true, if a new value is recorded.
    /// </summary>
    /// <returns>True if new value, else false</returns>
    private bool SendEveryBlink()
    {
        var data = TobiiXR.GetEyeTrackingData(TobiiXR_TrackingSpace.Local);
        var oldEyeState = eyeState;
        var currentEyeState = GetEyeState(data);
        if(Mode != TrackingSystemMode.Idle)
        {
            if(DetectBlink(oldEyeState, currentEyeState))
            {
                if (!CheckAmount())
                {
                    return false;
                }
                CollectData(data);
                return true;
            }
        }
        if(currentEyeState != EyeState.UNDEFINED) // Undefined is skipped
        {
            eyeState = currentEyeState;
        }
        return false;
    }

    /// <summary>
    /// Determines, if the user is blinking by the given eyestates.
    /// </summary>
    /// <param name="old">Eye state of the previous frame</param>
    /// <param name="current">Eye state of the current frame</param>
    /// <returns>True if blinking, false if not</returns>
    private bool DetectBlink(EyeState old, EyeState current)
    {
        switch(old)
        {
            case EyeState.OPEN:
            {
                if(current == EyeState.CLOSED && blinkState != BlinkState.OPEN_TO_CLOSED)
                {
                    blinkState = BlinkState.OPEN_TO_CLOSED;
                    return true;
                }
                break;
            }
            case EyeState.CLOSED:
            {
                if (current == EyeState.OPEN && blinkState != BlinkState.CLOSED_TO_OPEN)
                {
                    blinkState = BlinkState.CLOSED_TO_OPEN;
                    return true;
                }
                break;
            }
        }
        return false;
    }

    /// <summary>
    /// Checks if the amount of values is overstept and the mode should be changed.
    /// Also, once called, decrements the current amount of values.
    /// If the current amount of values is 0, the callback method to determine the new mod is invoked.
    /// </summary>
    /// <returns>True if amount is not 0, else false</returns>
    private bool CheckAmount()
    {
        if (AmountValues == 0)
        {
            if (!SetMode(Callback.Invoke(Mode)))
            {
                Mode = TrackingSystemMode.Idle;
                Debug.LogWarning("Callback to change mode failed - maybe cause of invalid arguments. Tracker " + Endpoint + "now in idle.");
            }
            return false;
        }
        else
        {
            if (AmountValues != -1)
            {
                AmountValues--;
            }
            return true;
        }
    }

    /// <summary>
    /// Checks if the system can be updated in the current interval.
    /// Also, once called, decrements the current restinterval by the given deltatime.
    /// If the current restinterval is below or equal 0, true will be returned and the interval is reset.
    /// </summary>
    /// <param name="deltaTime">DeltaTime of the previous frame</param>
    /// <returns>True if restinterval is below or equal 0, else false</returns>
    private bool CheckInterval(float deltaTime)
    {
        restInterval -= (int)(deltaTime * 1000);
        if (restInterval > 0)
        {
            return false;
        }
        else
        {
            restInterval = Interval - Math.Abs(restInterval);
            return true;
        }

    }

    /// <summary>
    /// Helper method to retrive data from the given SDK.
    /// Can be rewritten to use SR.Anipal SDK.
    /// </summary>
    /// <param name="data">SDK data to interact with the sensor</param>
    private void CollectData(TobiiXR_EyeTrackingData data)
    {
        leftEyeBlinking = data.IsLeftEyeBlinking;
        rightEyeBlinking = data.IsRightEyeBlinking;
        CurrentResult = new TrackingSystemResult(JsonUtil.ParseAsJson(new JsonBoolSet(leftEyeBlinking, rightEyeBlinking)), Endpoint, Mode);
    }

    /// <summary>
    /// Gets the current eye state from the given SDK data.
    /// Can be rewritten to use SR.Anipal SDK.
    /// </summary>
    /// <param name="data">SDK data to interact with the sensor</param>
    /// <returns>EyeState resulting from the sensor data</returns>
    private EyeState GetEyeState(TobiiXR_EyeTrackingData data)
    {
        if (data.IsLeftEyeBlinking && data.IsRightEyeBlinking)
        {
            return EyeState.CLOSED;
        }
        else if (!data.IsLeftEyeBlinking && !data.IsRightEyeBlinking)
        {
            return EyeState.OPEN;
        }
        else
        {
            return EyeState.UNDEFINED;
        }
    }

    /// <summary>
    /// Checks, if the given eyestates are different (simple: return old == current)
    /// </summary>
    /// <param name="old">Eyestate from previous frame</param>
    /// <param name="current">Eyestate from current frame</param>
    /// <returns>True if same, else false</returns>
    private bool DidStateChange(EyeState old, EyeState current)
    {
        return old == current;
    }
}

/// <summary>
/// Helper class to represent different eye states.
/// Undefined is needed for every ambigous eye state.
/// </summary>
public enum EyeState
{
    UNDEFINED, //One eye open
    OPEN,
    CLOSED
}

/// <summary>
/// Helper class for different blink states to help preventing duplicate blinks
/// </summary>
public enum BlinkState
{
    NONE,
    OPEN_TO_CLOSED,
    CLOSED_TO_OPEN
}
