﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base json class used for serialisation and communicating with the api
/// </summary>
public abstract class JsonObject  
{
    
}

/// <summary>
/// Json class representing a simple token
/// </summary>
public class JsonUserToken : JsonObject
{
    /// <summary>
    /// Servertoken
    /// </summary>
    public string Token { get; set; }
}

/// <summary>
/// Json class representing a simple floating tracking value
/// Used by: <see cref="SBloodPressureTrackingSystem"/>, <see cref="SHeartFrequencyTrackingSystem"/>, <see cref="SSkinSalvationTrackingSystem"/>, <see cref="STachygastricTrackingSystem"/> and <see cref="STemperatureTrackingSystem"/>
/// </summary>
public class JsonFloat : JsonObject
{
    /// <summary>
    /// Tracking value
    /// </summary>
    public float value { get; set; }

    /// <summary>
    /// Timestamp (UTC), when the value was pulled.
    /// </summary>
    public DateTime timestamp { get; set; }

    /// <summary>
    /// Instanciates a new JsonFloat with the given value and current timestamp
    /// </summary>
    /// <param name="value">Trackingdata value</param>
    public JsonFloat(float value)
    {
        this.value = value;
        timestamp = DateTime.UtcNow;
    }
}

/// <summary>
/// Json class representing a simple vector tracking value
/// Used by: <see cref="EyeBoundsTrackingSystem"/>
/// </summary>
public class JsonVector : JsonObject
{
    /// <summary>
    /// Tracking value (X-Component)
    /// </summary>
    public float x { get; set; }
    /// <summary>
    /// Tracking value (Y-Component)
    /// </summary>
    public float y { get; set; }
    /// <summary>
    /// Tracking value (Z-Component)
    /// </summary>
    public float z { get; set; }
    /// <summary>
    /// Timestamp (UTC), when the value was pulled.
    /// </summary>
    public DateTime timestamp { get; set; }

    /// <summary>
    /// Instanciates a new JsonVector with the given value and current timestamp
    /// </summary>
    /// <param name="vector">Trackingdata value</param>
    public JsonVector(Vector3 vector)
    {
        x = vector.x;
        y = vector.y;
        z = vector.z;
        timestamp = DateTime.UtcNow;
    }
}

/// <summary>
/// Json class representing a vectorset as tracking value
/// Used by: <see cref="EyePositionalTrackingSystem"/>
/// </summary>
public class JsonVectorSet : JsonObject
{
    /// <summary>
    /// First Vector Tracking value (X-Component)
    /// </summary>
    public float x1 { get; set; }
    /// <summary>
    /// First Vector Tracking value (Y-Component)
    /// </summary>
    public float y1 { get; set; }
    /// <summary>
    /// First Vector Tracking value (Z-Component)
    /// </summary>
    public float z1 { get; set; }
    /// <summary>
    /// Second Vector Tracking value (X-Component)
    /// </summary>
    public float x2 { get; set; }
    /// <summary>
    /// Second Vector Tracking value (Y-Component)
    /// </summary>
    public float y2 { get; set; }
    /// <summary>
    /// Second Vector Tracking value (Z-Component)
    /// </summary>
    public float z2 { get; set; }
    /// <summary>
    /// Timestamp (UTC), when the value was pulled.
    /// </summary>
    public DateTime timestamp { get; set; }

    /// <summary>
    /// Instanciates a new JsonVectorSet with the given values and current timestamp.
    /// </summary>
    /// <param name="vector1">Trackingdata vector1</param>
    /// <param name="vector2">Trackingdata vector2</param>
    public JsonVectorSet(Vector3 vector1, Vector3 vector2)
    {
        x1 = vector1.x;
        y1 = vector1.y;
        z1 = vector1.z;
        x2 = vector2.x;
        y2 = vector2.y;
        z2 = vector2.z;
        timestamp = DateTime.UtcNow;
    }
}

/// <summary>
/// Json class representing a simple boolean tracking value
/// Used by: <see cref="SBreathTrackingSystem"/>
/// </summary>
public class JsonBool : JsonObject
{
    /// <summary>
    /// Tracking value
    /// </summary>
    public bool value { get; set; }
    /// <summary>
    /// Timestamp (UTC), when the value was pulled.
    /// </summary>
    public DateTime timestamp { get; set; }

    /// <summary>
    /// Instanciates a new JsonBool with the given value and current timestamp.
    /// </summary>
    /// <param name="value">Trackingdata value</param>
    public JsonBool(bool value)
    {
        this.value = value;
        timestamp = DateTime.UtcNow;
    }

}

/// <summary>
/// Json class representing a booleanset as tracking value.
/// Used by: <see cref="EyeBlinkTrackingSystem"/>
/// </summary>
public class JsonBoolSet : JsonObject
{
    /// <summary>
    /// First Tracking value (left eye)
    /// </summary>
    public bool left { get; set; }
    /// <summary>
    /// Second Tracking value (right eye)
    /// </summary>
    public bool right { get; set; }
    /// <summary>
    /// Timestamp (UTC), when the value was pulled.
    /// </summary>
    public DateTime timestamp { get; set; }

    /// <summary>
    /// Instanciates a new JsonBoolSet with the given values and current timestamp.
    /// </summary>
    /// <param name="left">Trackingdata left eye</param>
    /// <param name="right">Trackingdata right eye</param>
    public JsonBoolSet(bool left, bool right)
    {
        this.left = left;
        this.right = right;
        timestamp = DateTime.UtcNow;
    }

}

/// <summary>
/// Json class representing a yawpitchrollset as tracking value.
/// Used by: <see cref="HeadMovementTrackingSystem"/>
/// </summary>
public class JsonYawPitchRollSet : JsonObject
{
    /// <summary>
    /// First Tracking value (yaw)
    /// </summary>
    public float yaw1 { get; set; }
    /// <summary>
    /// First Tracking value (pitch)
    /// </summary>
    public float pitch1 { get; set; }
    /// <summary>
    /// First Tracking value (roll)
    /// </summary>
    public float roll1 { get; set; }
    /// <summary>
    /// Second Tracking value (yaw)
    /// </summary>
    public float yaw2 { get; set; }
    /// <summary>
    /// Second Tracking value (pitch)
    /// </summary>
    public float pitch2 { get; set; }
    /// <summary>
    /// Second Tracking value (roll)
    /// </summary>
    public float roll2 { get; set; }
    /// <summary>
    /// Timestamp (UTC), when the value was pulled.
    /// </summary>
    public DateTime timestamp { get; set; }

    /// <summary>
    /// Instanciates a new JsonYawPitchRollSet with the given values and current timestamp.
    /// </summary>
    /// <param name="vector1">Trackingdata vector1</param>
    /// <param name="vector2">Trackingdata vector2</param>
    public JsonYawPitchRollSet(Vector3 vector1, Vector3 vector2)
    {
        yaw1 = vector1.x;
        pitch1 = vector1.y;
        roll1 = vector1.z;
        yaw2 = vector2.x;
        pitch2 = vector2.y;
        roll2 = vector2.z;
        timestamp = DateTime.UtcNow;
    }
}

/// <summary>
/// Json class representing a session info object
/// </summary>
public class JsonSessionInfo : JsonObject
{
    /// <summary>
    /// ID of the session
    /// </summary>
    public int ID { get; set; }
    /// <summary>
    /// User ID in the session
    /// </summary>
    public int ID_User { get; set; }
    /// <summary>
    /// Start Time of the session
    /// </summary>
    public DateTime Start_DateTime { get; set; }
    /// <summary>
    /// Current PR Value of the session
    /// </summary>
    public float Current_Pr_Value { get; set; }
    /// <summary>
    /// Interaction shortcuts of the session
    /// </summary>
    public string[] Session_Interaction_Shortcut { get; set; }
    /// <summary>
    /// Active device list of the session
    /// </summary>
    public string[] Session_Devices { get; set; }
    /// <summary>
    /// Threshold of the session
    /// </summary>
    public float Threshold { get; set; }
}

/// <summary>
/// Json class representing a pr_value object
/// </summary>
public class JsonPrValue : JsonObject
{
    /// <summary>
    /// Current PR Value of the session
    /// </summary>
    public float current_pr_value { get; set; }
}
/// <summary>
/// Json class respresenting one interaction shortcut
/// </summary>
public class JsonInteractionShortcut : JsonObject
{
    /// <summary>
    /// ID of the session
    /// </summary>
    public int ID { get; set; }
    /// <summary>
    /// User ID in the session
    /// </summary>
    public int ID_User { get; set; }
    /// <summary>
    /// Interaction shortcut
    /// </summary>
    public string Shortcut { get; set; }

}
/// <summary>
/// Json class respresenting sessiondevices
/// </summary>
public class JsonSessionDevices : JsonObject
{
    /// <summary>
    /// ID of the session
    /// </summary>
    public int ID_Session { get; set; }
    /// <summary>
    /// Session devices
    /// </summary>
    public string Device { get; set; }
}

/// <summary>
/// Json class respresenting sensor settings for every sensor on serverside
/// </summary>
public abstract class JsonSensorSettings : JsonObject
{
    /// <summary>
    /// Comparison table to determine pr value on server
    /// </summary>
    public Dictionary<float, float> PrValueTable { get; set; }
    /// <summary>
    /// Weight of the sensor
    /// </summary>
    public float weight { get; set; }
    /// <summary>
    /// Comparison value
    /// </summary>
    public float CompareValue { get; set; }
    /// <summary>
    /// True -> Wont override <see cref="CompareValue"/> on server with additional calibration values.
    /// </summary>
    public bool ConstantCompare { get; set;}
}

/// <summary>
/// Json class respresenting sensor settings for the <see cref="EyePositionalTrackingSystem"/>
/// </summary>
public class JsonEyeDistanceSettings : JsonSensorSettings
{
    /// <summary>
    /// Distance measurement mode
    /// </summary>
    public DistanceMode Mode { get; set; }
}

/// <summary>
/// Json class respresenting sensor settings for the <see cref="EyeBoundsTrackingSystem"/>
/// </summary>
public class JsonEyeBoundsSettings : JsonSensorSettings
{
    /// <summary>
    /// Distance measurement mode
    /// </summary>
    public DistanceMode Mode { get; set; }
}

/// <summary>
/// Json class respresenting sensor settings for the <see cref="EyeBlinkTrackingSystem"/>
/// </summary>
public class JsonEyeBlinkSettings : JsonSensorSettings
{

}

/// <summary>
/// Json class respresenting sensor settings for the <see cref="HeadMovementTrackingSystem"/>
/// </summary>
public class JsonHeadMovementSettings : JsonSensorSettings
{

}

