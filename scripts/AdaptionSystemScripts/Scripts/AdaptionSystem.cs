﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEngine.Windows;

/// <summary>
/// Core class of the adaption system. Should be attached to a gameobject.
/// Controls and manages the whole system and provides an interface via the unity inspector and functions.
/// </summary>
public class AdaptionSystem : MonoBehaviour
{
	/// <summary>
	/// Inspector Attribute: Enables the <see cref="AdaptionManager"/>
	/// </summary>
	[Tooltip("Enables communication with webserver regarding adaption")]
	public bool EnableAdaption;
	/// <summary>
	/// Inspector Attribute: Interval in which the adaption manager requests the prvalue
	/// </summary>
	[ConditionalHide("EnableAdaption", true)]
	[Tooltip("Interval (in ms) in which the current pr vlaue is retrieved from the webserver")]
	public int IntervalPrValue;

	/// <summary>
	/// Inspector Attribute: Launches the htc vive calibration utility
	/// </summary>
	[Tooltip("Launches the HTC VIVE Eye Calibration Tool on start. Should be done, when hmd is used for the first time and after some periods to recalibrate")]
	public bool LaunchViveEyeCalibrationTool;
	/// <summary>
	/// Inspector Attribute: Shows a circle, whcih represents the current gaze of the eyes
	/// </summary>
	[Tooltip("Shows a round circle, which indicates where the user is looking. Requires an eye tracking camera.")]
	public bool ShowEyeGaze;

	/// <summary>
	/// Inspector Attribute: Switches system to have one global interval value for every tracking system
	/// </summary>
	[Tooltip("Uses a global interval for all tracking systems.")]
	public bool UseGlobalInterval;
	/// <summary>
	/// Inspector Attribute: Interval, in which every tracking system should get an update
	/// </summary>
	[ConditionalHide("UseGlobalInterval", true)]
	[Tooltip("Global Interval in ms.")]
	public int GlobalInterval;
	/// <summary>
	/// Inspector Attribute: Switches system to have one global amount of values for every tracking system
	/// </summary>
	[Tooltip("Uses global calibration values for all tracking systems.")]
	public bool UseGlobalCalibrationValues;
	/// <summary>
	/// Inspector Attribute: Amount of values for every tracking system, after which each system should change its mode
	/// </summary>
	[ConditionalHide("UseGlobalCalibrationValues", true)]
	[Tooltip("Amount of calibration vlaues for each active tracking system.")]
	public int CalibrationValues;

	/// <summary>
	/// Inspector Attribute: Enables the eye distance tracking system
	/// </summary>
	[Header("EyeDistanceTracking")]
	[Tooltip("Enables Eye Positional Distance Tracking")]
	public bool EnableEyeDistanceTracking;
	/// <summary>
	/// Inspector Attribute: Filename in <see cref="AdaptionSystem.SETTINGSFILEFOLDER"/> for the tracking system
	/// </summary>
	[ConditionalHide("EnableEyeDistanceTracking")]
	[Tooltip("Json File name for specific serverside tracker settings")]
	public string UseEyeDistanceSettings;
	/// <summary>
	/// Inspector Attribute: Endpoint on the kinetosis webserver for the tracking system
	/// </summary>
	[ConditionalHide("EnableEyeDistanceTracking")]
	[Tooltip("Name of the tracker on serverside")]
	public string DistanceTrackerEndpoint;
	/// <summary>
	/// Inspector Attribute: Interval for the tracking system rregarding updating frequency
	/// </summary>
	[ConditionalHide(ConditionalSourceFields = new string[] { "EnableEyeDistanceTracking", "UseGlobalInterval" }, ConditionalSourceFieldInverseBools = new bool[] { false, true })]
	[Tooltip("Interval in ms.")]
	public int IntervalEyeDistance;
	/// <summary>
	/// Inspector Attribute: Amount fo values for the tracking system after which it should change his mode
	/// </summary>
	[ConditionalHide(ConditionalSourceFields = new string[] { "EnableEyeDistanceTracking", "UseGlobalCalibrationValues" }, ConditionalSourceFieldInverseBools = new bool[] { false, true })]
	[Tooltip("Amount of calibration vlaues for the tracking system.")]
	public int DistanceInitialCalibrationValues;

	/// <summary>
	/// Inspector Attribute: Enables the eyebounds tracking system
	/// </summary>
	[Header("EyeBoundsTracking")]
	[Tooltip("Enables Eye Bounds Distance Tracking")]
	public bool EnableEyeBoundsTracking;
	/// <summary>
	/// Inspector Attribute: Filename in <see cref="AdaptionSystem.SETTINGSFILEFOLDER"/> for the tracking system
	/// </summary>
	[ConditionalHide("EnableEyeBoundsTracking")]
	[Tooltip("Json File name for specific serverside tracker settings")]
	public string UseEyeBoundsSettings;
	/// <summary>
	/// Inspector Attribute: Endpoint on the kinetosis webserver for the tracking system
	/// </summary>
	[ConditionalHide("EnableEyeBoundsTracking")]
	[Tooltip("Name of the tracker on serverside")]
	public string BoundsTrackerEndpoint;
	/// <summary>
	/// Inspector Attribute: Interval for the tracking system rregarding updating frequency
	/// </summary>
	[ConditionalHide(ConditionalSourceFields = new string[] { "EnableEyeBoundsTracking", "UseGlobalInterval" }, ConditionalSourceFieldInverseBools = new bool[] { false, true })]
	[Tooltip("Interval in ms.")]
	public int IntervalEyeBounds;
	/// <summary>
	/// Inspector Attribute: Amount fo values for the tracking system after which it should change his mode
	/// </summary>
	[ConditionalHide(ConditionalSourceFields = new string[] { "EnableEyeBoundsTracking", "UseGlobalCalibrationValues" }, ConditionalSourceFieldInverseBools = new bool[] { false, true })]
	[Tooltip("Amount of calibration vlaues for the tracking system.")]
	public int BoundsInitialCalibrationValues;

	/// <summary>
	/// Inspector Attribute: Enables the eye blink tracking system
	/// </summary>
	[Header("EyeBlinkTracking")]
	[Tooltip("Enables Eye Blink Tracking")]
	public bool EnableBlinkTracking;
	/// <summary>
	/// Inspector Attribute: Filename in <see cref="AdaptionSystem.SETTINGSFILEFOLDER"/> for the tracking system
	/// </summary>
	[ConditionalHide("EnableBlinkTracking")]
	[Tooltip("Json File name for specific serverside tracker settings")]
	public string UseEyeBlinkSettings;
	/// <summary>
	/// Inspector Attribute: Endpoint on the kinetosis webserver for the tracking system
	/// </summary>
	[ConditionalHide("EnableBlinkTracking")]
	[Tooltip("Name of the tracker on serverside")]
	public string BlinkTrackerEndpoint;
	/// <summary>
	/// Inspector Attribute: Changes the blink tracker updating function to recognize every blink if possible
	/// </summary>
	[ConditionalHide("EnableBlinkTracking")]
	[Tooltip("Ignores the given interval and tracks every blink.")]
	public bool TrackEveryBlink;
	/// <summary>
	/// Inspector Attribute: Interval for the tracking system rregarding updating frequency
	/// </summary>
	[ConditionalHide(ConditionalSourceFields = new string[] { "EnableBlinkTracking", "UseGlobalInterval" }, ConditionalSourceFieldInverseBools = new bool[] { false, true })]
	[Tooltip("Interval in ms.")]
	public int IntervalEyeBlinks;
	/// <summary>
	/// Inspector Attribute: Amount fo values for the tracking system after which it should change his mode
	/// </summary>
	[ConditionalHide(ConditionalSourceFields = new string[] { "EnableBlinkTracking", "UseGlobalCalibrationValues" }, ConditionalSourceFieldInverseBools = new bool[] { false, true })]
	[Tooltip("Amount of calibration vlaues for the tracking system.")]
	public int BlinkInitialCalibrationValues;

	/// <summary>
	/// Inspector Attribute: Enables the head movement tracking system
	/// </summary>
	[Header("HeadMovementTracking")]
	[Tooltip("Enables Head Movement Tracking")]
	public bool EnableHeadMovementTracking;
	/// <summary>
	/// Inspector Attribute: Filename in <see cref="AdaptionSystem.SETTINGSFILEFOLDER"/> for the tracking system
	/// </summary>
	[ConditionalHide("EnableHeadMovementTracking")]
	[Tooltip("Json File name for specific serverside tracker settings")]
	public string UseHeadMovementSettings;
	/// <summary>
	/// Inspector Attribute: Endpoint on the kinetosis webserver for the tracking system
	/// </summary>
	[ConditionalHide("EnableHeadMovementTracking")]
	[Tooltip("Name of the tracker on serverside")]
	public string HeadTrackerEndpoint;
	/// <summary>
	/// Inspector Attribute: Interval for the tracking system rregarding updating frequency
	/// </summary>
	[ConditionalHide(ConditionalSourceFields = new string[] { "EnableHeadMovementTracking", "UseGlobalInterval" }, ConditionalSourceFieldInverseBools = new bool[] { false, true })]
	[Tooltip("Interval in ms.")]
	public int IntervalHeadMovement;
	/// <summary>
	/// Inspector Attribute: Amount fo values for the tracking system after which it should change his mode
	/// </summary>
	[ConditionalHide(ConditionalSourceFields = new string[] { "EnableHeadMovementTracking", "UseGlobalCalibrationValues" }, ConditionalSourceFieldInverseBools = new bool[] { false, true })]
	[Tooltip("Amount of calibration vlaues for the tracking system.")]
	public int HeadInitialCalibrationValues;

	/// <summary>
	/// Inspector Attribute: Enables all simulated tracking systems
	/// </summary>
	[Header("SimulatedTrackers")]
	[Tooltip("Enables all simulated trackers.")]
	public bool EnableSimulatedTracker;
	/// <summary>
	/// Inspector Attribute: Interval for the tracking systems rregarding updating frequency
	/// </summary>
	[ConditionalHide(ConditionalSourceFields = new string[] { "EnableSimulatedTracker", "UseGlobalInterval" }, ConditionalSourceFieldInverseBools = new bool[] { false, true })]
	[Tooltip("Interval in ms.")]
	public int IntervalSimulatedTracker;
	/// <summary>
	/// Inspector Attribute: Amount fo values for the tracking systems after which they should change their mode
	/// </summary>
	[ConditionalHide(ConditionalSourceFields = new string[] { "EnableSimulatedTracker", "UseGlobalCalibrationValues" }, ConditionalSourceFieldInverseBools = new bool[] { false, true })]
	[Tooltip("Amount of calibration vlaues for the tracking systems.")]
	public int SimulatedInitialCalibrationValues;

	/// <summary>
	/// Contains the ressource path for the sensorsettings
	/// </summary>
	[HideInInspector]
	public const string SETTINGSFILEFOLDER = "Assets/AdaptionSystemScripts/SensorSettings/";

	/// <summary>
	/// Storage for the <see cref="KinetosisServerAPI"/>
	/// </summary>
	private KinetosisServerAPI Api;
	/// <summary>
	/// Storage for the <see cref="JsonSessionInfo"/>
	/// Will be loaded once and used to recognize the adaption threshold.
	/// </summary>
	private JsonSessionInfo Info;
	/// <summary>
	/// Store for the <see cref="AdaptionManager"/>
	/// Manages, if active, everything related to pr comparison and adpation/interaction possibilities
	/// </summary>
	[HideInInspector]
	public AdaptionManager Manager
    {
		get; 
		private set;
    }
	/// <summary>
	/// Store for the <see cref="TrackingSystemManager"/>
	/// Manages all active tracking systems
	/// </summary>
	[HideInInspector]
	public TrackingSystemManager TrackingSystems
    {
		get; 
		private set;
    }

	void Awake()
	{
		TrackingSystems = new TrackingSystemManager();
		Api = GetComponent<KinetosisServerAPI>();
		Manager = new AdaptionManager(IntervalPrValue, Api, StandardOnThresholdOver, StandardOnThresholdBack);
		if(LaunchViveEyeCalibrationTool)
        {
			ViveSR.anipal.Eye.SRanipal_Eye_v2.LaunchEyeCalibration();
        }
	}

	void Start()
	{
		var gaze = transform.Find("GazeVisualizer").gameObject;
		if (ShowEyeGaze)
		{
			gaze.SetActive(true);
		}
		else
		{
			gaze.SetActive(false);
		}
		if (UseGlobalInterval)
        {
			IntervalEyeDistance = GlobalInterval;
			IntervalEyeBounds = GlobalInterval;
			IntervalEyeBlinks = GlobalInterval;
			IntervalHeadMovement = GlobalInterval;
			IntervalSimulatedTracker = GlobalInterval;
		}

		if(UseGlobalCalibrationValues)
        {
			DistanceInitialCalibrationValues = CalibrationValues;
			BoundsInitialCalibrationValues = CalibrationValues;
			BlinkInitialCalibrationValues = CalibrationValues;
			HeadInitialCalibrationValues = CalibrationValues;
			SimulatedInitialCalibrationValues = CalibrationValues;
		}

		if(TrackEveryBlink)
        {
			IntervalEyeBlinks = -1;
		}
		if (EnableEyeDistanceTracking)
		{
			TrackingSystems.AddTrackingSystem(new EyePositionalTrackingSystem(DistanceTrackerEndpoint, IntervalEyeDistance, StandardOnFinishInitial, DistanceInitialCalibrationValues, RecieveSettings(UseEyeDistanceSettings)));
		}
		if (EnableEyeBoundsTracking)
		{
			TrackingSystems.AddTrackingSystem(new EyeBoundsTrackingSystem(BoundsTrackerEndpoint, IntervalEyeBounds, StandardOnFinishInitial, BoundsInitialCalibrationValues, RecieveSettings(UseEyeBoundsSettings)));		
		}
		if (EnableBlinkTracking)
		{
			TrackingSystems.AddTrackingSystem(new EyeBlinkTracker(BlinkTrackerEndpoint, IntervalEyeBlinks, StandardOnFinishInitial, BlinkInitialCalibrationValues, RecieveSettings(UseEyeBlinkSettings)));
		}
		if (EnableHeadMovementTracking)
        {
			TrackingSystems.AddTrackingSystem(new HeadTrackingSystem(HeadTrackerEndpoint, IntervalHeadMovement, StandardOnFinishInitial, HeadInitialCalibrationValues, RecieveSettings(UseHeadMovementSettings)));
		}
		if(EnableSimulatedTracker)
        {
			var bpt = new SBloodPressureTrackingSystem("BloodPressureTracker", IntervalSimulatedTracker, StandardOnFinishInitial, SimulatedInitialCalibrationValues, JsonUtil.JSONEMPTY);
			var bt = new SBreathTrackingSystem("BreathTracker", IntervalSimulatedTracker, StandardOnFinishInitial, SimulatedInitialCalibrationValues, JsonUtil.JSONEMPTY);
			var hft = new SHeartFrequencyTracker("HeartFrequencyTracker", IntervalSimulatedTracker, StandardOnFinishInitial, SimulatedInitialCalibrationValues, JsonUtil.JSONEMPTY);
			var tt = new STemperatureTrackingSystem("TemperatureTracker", IntervalSimulatedTracker, StandardOnFinishInitial, SimulatedInitialCalibrationValues, JsonUtil.JSONEMPTY);
			var sst= new SSkinSalvationTrackingSystem("SkinSalvationTracker", IntervalSimulatedTracker, StandardOnFinishInitial, SimulatedInitialCalibrationValues, JsonUtil.JSONEMPTY);
			var tat = new STachygastricTrackingSystem("TachygastricTracker", IntervalSimulatedTracker, StandardOnFinishInitial, SimulatedInitialCalibrationValues, JsonUtil.JSONEMPTY);

			//Breathracker should be controlled manually - Experimentall
			bt.SetMode(new TrackingSystemModeState() { AmountValues = -1, Mode = TrackingSystemMode.Idle });

			TrackingSystems.AddTrackingSystem(bpt);
			TrackingSystems.AddTrackingSystem(bt);
			TrackingSystems.AddTrackingSystem(hft);
			TrackingSystems.AddTrackingSystem(tt);
			TrackingSystems.AddTrackingSystem(sst);
			TrackingSystems.AddTrackingSystem(tat);
		}

		Api.LogIn(OnLogin);

	}

	//Main loop
    void Update()
	{
		var deltaTime = Time.deltaTime;
		if (!Api.LoggedIn)
		{
			return;
		}
		TrackingSystems.Update(deltaTime);
		while (TrackingSystems.RecentValues.Count > 0)
		{
			var current = TrackingSystems.RecentValues.Dequeue();
			switch (current.Mode)
			{
				case TrackingSystemMode.Idle:
					break;
				case TrackingSystemMode.Calibrating:
					Api.SendToDeviceCalibrate(current.Owner, current.ValueAsJson, OnCalibrate);
					break;
				case TrackingSystemMode.Tracking:
					Api.SendToDevice(current.Owner, current.ValueAsJson, OnTrack);
					break;
			}
		}
		if (EnableAdaption)
		{
			Manager.Update(deltaTime);
		}
	}

	/// <summary>
	/// Retrieves the settings string from the given filename.
	/// if any errors occur, an empty json string is given back.
	/// </summary>
	/// <param name="settingsFile">Filename</param>
	/// <returns>Json String</returns>
	private string RecieveSettings(string settingsFile)
	{
		if (!settingsFile.Equals(""))
		{
			if (!settingsFile.Trim().EndsWith(".json"))
			{
				settingsFile += ".json";
			}
			var path = SETTINGSFILEFOLDER + settingsFile;
			if (System.IO.File.Exists(path))
			{
				return System.IO.File.ReadAllText(path);
			}
			Debug.LogWarning("Settings file not found: " + path);
		}
		return JsonUtil.JSONEMPTY;
	}

	/// <summary>
	/// Standard callback for each tracking system. Can be used from outside.
	/// </summary>
	/// <param name="from">Old mode of the tracking system</param>
	/// <returns><see cref="TrackingSystemModeState"/> containing the next mode and the amount of values</returns>
	public static TrackingSystemModeState StandardOnFinishInitial(TrackingSystemMode from)
    {
        switch (from)
        {
			case TrackingSystemMode.Idle: //Idle finished  -> next mode: Idle
				return new TrackingSystemModeState()
				{
					AmountValues = -1,
					Mode = TrackingSystemMode.Idle
				};
			case TrackingSystemMode.Calibrating: //Calibrating finished  -> next mode: Tracking
				return new TrackingSystemModeState()
				{
					AmountValues = -1,
					Mode = TrackingSystemMode.Tracking
				};
			case TrackingSystemMode.Tracking: // Tracking finished -> next mode: Idle
				return new TrackingSystemModeState()
				{
					AmountValues = -1,
					Mode = TrackingSystemMode.Idle
				};
		}
		return new TrackingSystemModeState() // Shouldn't be reached
		{
			AmountValues = -1,
			Mode = TrackingSystemMode.Idle
		};
	}

	/// <summary>
	/// Standard callback for the <see cref="AdaptionManager"/> when exceeding the threshold
	/// </summary>
	/// <param name="currentValue">Current PR Value</param>
	/// <param name="suggestion"><see cref="APIInteraction"/> containing interaction shortcuts</param>
	private void StandardOnThresholdOver(float currentValue, APIInteraction suggestion)
    {
		Debug.Log("Reached threshold! Current Value: " + currentValue);
		Debug.LogWarning("Simulating keypresses not supported in Unity. Printing the keypresses:");
		int i = 1;
		foreach(var element in suggestion.InteractionSuggestions)
        {
			Debug.Log("Interaction #" + i + ": " + element);
			i++;
        }
    }

	/// <summary>
	/// Standard callback for the <see cref="AdaptionManager"/> when falling below the threshold
	/// </summary>
	/// <param name="currentValue">Current PR Value</param>
	/// <param name="suggestion"><see cref="APIInteraction"/> containing interaction shortcuts</param>
	private void StandardOnThresholdBack(float currentValue, APIInteraction suggestion)
	{
		Debug.Log("Threshold not reached anymore! CurrentValue: " + currentValue);
	}

	/// <summary>
	/// Used to overwrite the standard callback <see cref="AdaptionSystem.StandardOnThresholdOver(float, APIInteraction)"/> for the <see cref="AdaptionManager"/> with the given Callback.
	/// </summary>
	/// <param name="callback">Overwriting callback (see signature: <see cref="AdaptionManager.OnThreshold"/>)</param>
	public void OverwriteOnTreshholdOver(AdaptionManager.OnThreshold callback)
    {
		Manager.CallbackReached = callback;
    }

	/// <summary>
	/// Used to overwrite the standard callback <see cref="AdaptionSystem.StandardOnThresholdBack(float, APIInteraction)"/> for the <see cref="AdaptionManager"/> with the given Callback.
	/// </summary>
	/// <param name="callback">Overwriting callback (see signature: <see cref="AdaptionManager.OnThreshold"/>)</param>
	public void OverwriteOnTreshholdBack(AdaptionManager.OnThreshold callback)
	{
		Manager.CallbackBack = callback;
	}

	/// <summary>
	/// Standard callback once the system got an answer from the server regarding authentication (<see cref="KinetosisServerAPI.LogIn(KinetosisServerAPI.ApiCallBack)"/>).
	/// </summary>
	/// <param name="response">Answer of the server</param>
	private void OnLogin(APIResponse response)
	{
		if (response.Succeded && Api.LoggedIn)
		{
			RetriveSessionInfo();
			TrackingSystems.SendSettings(Api, OnSettingsSend);
		}
		else
		{
			TrackingSystems.SleepAll();
			Debug.LogError("Couldnt connect to server.");
			Debug.LogError("Manually connect and configure systems.");
		}
	}
	
	/// <summary>
	/// Standard callback once the server accepted calibrating data and the system recieved an answer <see cref="KinetosisServerAPI.SendToDeviceCalibrate(string, string, KinetosisServerAPI.ApiCallBack)"/>
	/// Empty cause unused, but can be filled with debug information or future extensions.
	/// </summary>
	/// <param name="response">Server response</param>
	[Obsolete("Unused")]
	private void OnCalibrate(APIResponse response)
	{

	}

	/// <summary>
	/// Standard callback once the server accepted tracking data and the system recieved an answer <see cref="KinetosisServerAPI.SendToDevice(string, string, KinetosisServerAPI.ApiCallBack)"/>
	/// Empty cause unused, but can be filled with debug information or future extensions.
	/// </summary>
	/// <param name="response">Server response</param>
	[Obsolete("Unused")]
	private void OnTrack(APIResponse response)
	{

	}

	/// <summary>
	/// Standard callback once the server accepted settings data and the system recieved an answer <see cref="KinetosisServerAPI.SendToDeviceSettings(string, string, KinetosisServerAPI.ApiCallBack)(string, string, KinetosisServerAPI.ApiCallBack)"/>
	/// Empty cause unused, but can be filled with debug information or future extensions.
	/// </summary>
	/// <param name="response">Server response</param>
	[Obsolete("Unused")]
	private void OnSettingsSend(APIResponse response)
	{

	}

	/// <summary>
	/// Sends a web request to the webserver to get the current session infos.
	/// Those will be stored in <see cref="AdaptionSystem.Info"/> and <see cref="AdaptionManager.Info"/> after the callback <see cref="AdaptionSystem.OnSession(APIResponse)"/> was called.
	/// </summary>
	public void RetriveSessionInfo()
	{
		Api.RecieveoFromSession(OnSession);
	}

	/// <summary>
	/// Standard callback for incoming session information (see <see cref="KinetosisServerAPI.RecieveoFromSession(KinetosisServerAPI.ApiCallBack)"/>)
	/// Gives the session info to the <see cref="AdaptionManager.Info"/> and <see cref="AdaptionSystem.Info"/>
	/// </summary>
	/// <param name="response">Server response</param>
	private void OnSession(APIResponse response)
	{
		if (response.Succeded)
		{
			if (response.IsJson)
			{
				Info = JsonUtil.ParseAsObject<JsonSessionInfo>(response.APIResponseContent);
				Manager.Info = Info;
			}
			else
			{
				Debug.LogError("Invalid Server response:");
				Debug.Log(response.APIResponseContent);
			}
		}
		else
		{
			Debug.LogError("Session Info couldnt be retrieved.");
		}
	}

}

