﻿using System;
using System.Collections;
using System.Collections.Generic;
using Tobii.XR;
using UnityEngine;

/// <summary>
/// Sensor system for the simulated breath tracker.
/// Retrieves values about the current value as a boolean.
/// </summary>
public class SBreathTrackingSystem : STrackingSystem
{
    /// <summary>
    /// Rest interval to calculate the current time delta
    /// </summary>
    private int restInterval;

    /// <summary>
    /// Storage for the current value.
    /// </summary>
    private bool inhale;

    /// <summary>
    /// Storage for the current breath state to predict breathing.
    /// </summary>
    private BreathState inhalteState;
     /// <summary>
    /// Storage for the current breath track state to prevent duplicate values.
    /// </summary>
    private BreathTrackState changeState;

    /// <summary>
    /// Instanciates a new simulated breath tracker with the given values.
    /// Calls <see cref="TrackingSystem(string endpoint, int interval, OnFinsihAmount callback = null, int amountValues = -1, string settings = null)"/> with the given values.
    /// Needs to get an unique signature when implemented
    /// </summary>
    /// <param name="endpoint">Endpoint of the sensor on server side</param>
    /// <param name="interval">Interval in ms, in which the sensor updates its values</param>
    /// <param name="callback">Callback method to determine next state</param>
    /// <param name="amountValues">Amount of values, after which the state should change</param>
    /// <param name="settings">Settings which this sensor should overwrite on the server</param>
    public SBreathTrackingSystem(string endpoint, int interval, OnFinsihAmount callback = null, int amountValues = -1, string settings = null)
        : base(endpoint, interval, callback, amountValues, settings)
    {
        Signature = typeof(SBreathTrackingSystem).Name;
        if (interval == -1)
        {
            restInterval = 0;
        }
        else
        {
            restInterval = interval;
        }

        if (AmountValues != -1 && AmountValues > 0)
        {
            AmountValues *= 2;
        }

        IsGeneratingRandom = false;

        inhale = false;

        changeState = BreathTrackState.NONE;
        inhalteState = BreathState.UNDEFINED;
    }

    /// <summary>
    /// Sets the steps to the new value.
    /// </summary>
    /// <param name="steps">New Value of the steps</param>
    public override void SetSteps(float steps)
    {
        Steps = steps;
    }

    /// <summary>
    /// Sets the current value to true.
    /// </summary>
    /// <return>Success/Failure</return>
    public override bool Increase()
    {
        inhale = true;
        return true;
    }

    /// <summary>
    /// Sets the current value to false.
    /// </summary>
    /// <return>Success/Failure</return>
    public override bool Decrease()
    {
        inhale = false;
        return false;
    }

    /// <summary>
    /// Sets the random generation lever.
    /// </summary>
    /// <param name="enable">Enable/Disable random generation</param>
    /// <param name="range">Range for random generation (ignored)</param>
    /// <return>Success/Failure</return>
    public override bool RandomGeneration(bool enable, float range = 0)
    {
        if (!enable)
        {
            IsGeneratingRandom = false;
        }
        else
        {
            if (range < 0)
            {
                return false;
            }
            IsGeneratingRandom = true;
        }
        return true;
    }

    /// <summary>
    /// Implemented function from base class to overwrite mode. 
    /// Should normally recieve the result of the callback as param. 
    /// </summary>
    /// <param name="mode">New mode with amount of values</param>
    /// <returns>Success/Failure</returns>
    public override bool SetMode(TrackingSystemModeState mode)
    {
        Mode = mode.Mode;
        if (mode.AmountValues < 0 && mode.AmountValues != -1)
        {
            Debug.LogWarning(mode.AmountValues.ToString() + " is not a valid argument for tracking system " + Signature + "! Changing to -1. Adjust manually.");
            this.AmountValues = -1;
            return false;
        }
        AmountValues = mode.AmountValues;
        return true;
    }

    /// <summary>
    /// Updates the tracking system in the given interval.
    /// Returns true, if a new value is recorded.
    /// </summary>
    /// <param name="deltaTime">TimeDelta to the previous frame</param>
    /// <returns>True if new value, else false</returns>
    public override bool UpdateValue(float deltaTime)
    {
        if (Interval == -1)
        {
            return SendEveryBreath();
        }
        if (!CheckInterval(deltaTime))
        {
            return false;
        }
        if (!CheckAmount())
        {
            return false;
        }
        if (Mode != TrackingSystemMode.Idle)
        {
            CollectData();
            return true;
        }
        return false;
    }

    /// <summary>
    /// Alternate function to detect every breath. (see <see cref="EyeBlinkTracker.DetectEveryBlink()"/>)
    /// Returns true, if a new value is recorded.
    /// </summary>
    /// <returns>True if new value, else false</returns>
    private bool SendEveryBreath()
    {
        if (IsGeneratingRandom)
        {
            inhale = UnityEngine.Random.Range(0, 2) == 0 ? true : false;
        }
        var oldInhaleState = inhalteState;
        var currentInhaleState = GetCurrentState();
        if (Mode != TrackingSystemMode.Idle)
        {
            if (DetectInhale(oldInhaleState, currentInhaleState))
            {
                if (!CheckAmount())
                {
                    return false;
                }
                CollectData();
                return true;
            }
        }
        if (currentInhaleState != BreathState.UNDEFINED)
        {
            inhalteState = currentInhaleState;
        }
        return false;
    }

    /// <summary>
    /// Checks if the breath state changed.
    /// </summary>
    /// <param name="old">Previous breath state</param>
    /// <param name="current">Current breath state</param>
    /// <returns>True if changed, else false</returns>
    private bool DetectInhale(BreathState old, BreathState current)
    {
        switch (old)
        {
            case BreathState.INHALING:
                {
                    if (current == BreathState.EXHALING && changeState != BreathTrackState.INHALING_TO_EXHALING)
                    {
                        changeState = BreathTrackState.INHALING_TO_EXHALING;
                        return true;
                    }
                    break;
                }
            case BreathState.EXHALING:
                {
                    if (current == BreathState.INHALING && changeState != BreathTrackState.EXHALING_ZO_INHALING)
                    {
                        changeState = BreathTrackState.EXHALING_ZO_INHALING;
                        return true;
                    }
                    break;
                }
        }
        return false;
    }

    /// <summary>
    /// Retrieves the current breath state from <see cref="inhale"/>
    /// </summary>
    /// <return>Corresponding breath state</return>
    private BreathState GetCurrentState()
    {
        if(inhale)
        {
            return BreathState.INHALING;
        }
        return BreathState.EXHALING;
    }

    /// <summary>
    /// Checks if the amount of values is overstept and the mode should be changed.
    /// Also, once called, decrements the current amount of values.
    /// If the current amount of values is 0, the callback method to determine the new mod is invoked.
    /// </summary>
    /// <returns>True if amount is not 0, else false</returns>
    private bool CheckAmount()
    {
        if (AmountValues == 0)
        {
            if (!SetMode(Callback.Invoke(Mode)))
            {
                Mode = TrackingSystemMode.Idle;
                Debug.LogWarning("Callback to change mode failed - maybe cause of invalid arguments. Tracker " + Endpoint + "now in idle.");
            }
            return false;
        }
        else
        {
            if (AmountValues != -1)
            {
                AmountValues--;
            }
            return true;
        }
    }

    /// <summary>
    /// Checks if the system can be updated in the current interval.
    /// Also, once called, decrements the current restinterval by the given deltatime.
    /// If the current restinterval is below or equal 0, true will be returned and the interval is reset.
    /// </summary>
    /// <param name="deltaTime">DeltaTime of the previous frame</param>
    /// <returns>True if restinterval is below or equal 0, else false</returns>
    private bool CheckInterval(float deltaTime)
    {
        restInterval -= (int)(deltaTime * 1000);
        if (restInterval > 0)
        {
            return false;
        }
        else
        {
            restInterval = Interval - Math.Abs(restInterval);
            return true;
        }

    }

    /// <summary>
    /// Helper method to retrive data.
    /// </summary>
    private void CollectData()
    {
        if (IsGeneratingRandom)
        {
            currentValue = UnityEngine.Random.Range(0, 2) == 0 ? true : false;
        }
        CurrentResult = new TrackingSystemResult(JsonUtil.ParseAsJson(new JsonBool(inhale)), Endpoint, Mode);
    }

}

/// <summary>
/// Helper class representing different breath states.
/// </summary>
public enum BreathState
{
    UNDEFINED,
    INHALING,
    EXHALING
}

/// <summary>
/// Helper class representing different changes between breath states.
/// </summary>
public enum BreathTrackState
{
    NONE,
    INHALING_TO_EXHALING,
    EXHALING_ZO_INHALING
}
