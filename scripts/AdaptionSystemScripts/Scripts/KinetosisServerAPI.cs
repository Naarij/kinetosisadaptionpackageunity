﻿using System;
using System.Collections;
using System.Net;
using System.Text;
using UnityEngine;
using UnityEngine.Networking;

/// <summary>
/// Interface for communication with the kinetosis webserver. Must be attached to a gameobject.
/// </summary>
public class KinetosisServerAPI : MonoBehaviour {
    /// <summary>
    /// Url of the server
    /// </summary>
    [Tooltip("Url of the kinetosis webserver")]
    public string Url;
    /// <summary>
    /// Username required for authentication
    /// </summary>
    [Tooltip("Username for the kinetosis webserver")]
    public string Username;
    /// <summary>
    /// Password required for authentication
    /// </summary>
    [Tooltip("Password for the kinetosis webserver")]
    public string Password;

    /// <summary>
    /// Token for communicating with the webserver (see <see href="https://tools.ietf.org/html/rfc2617"/> and <see href="https://tools.ietf.org/html/rfc6750"/>
    /// IMPORTANT: Token must be renewed after 2 hours
    /// </summary>
    private string Token;

    /// <summary>
    /// Fucntion signature regarding api callbacks for server responses
    /// </summary>
    /// <param name="response">Server response</param>
    public delegate void ApiCallBack(APIResponse response);

    /// <summary>
    /// Checks if the token is given. If yes, the systems assumes you are logged in.
    /// IMPORTANT: Token must be renewed after 2 hours, which isn't checked via this
    /// </summary>
    public bool LoggedIn
    {
        get
        {
            return Token != null;
        }
    }

    /// <summary>
    /// Creates an interface to communicate with the api with the given params.
    /// </summary>
    /// <param name="url">Server url</param>
    /// <param name="username">Username for authnetication</param>
    /// <param name="password">Password for authentication</param>
    [Obsolete("Instanciation works now via unity gameobject")]
    public KinetosisServerAPI(string url, string username, string password)
    {
        Url = url;
        Username = username;
        Password = password;
        Token = "";
    }

    /// <summary>
    /// Builds up a synced connection to the webserver and tries to authenticate.
    /// </summary>
    /// <returns>Succes/Failure</returns>
    [Obsolete("Outdated and shouldn't be used anymore. Doesnt work async and uses the old c# request library.")]
    public bool ConnectAndLogin()
    {
        //https://www.ibm.com/support/knowledgecenter/en/SSGMCP_5.1.0/com.ibm.cics.ts.internet.doc/topics/dfhtl2a.html
        var request = (HttpWebRequest)WebRequest.Create(Url+APIEndpoints.ENDPOINTTOKEN);
        request.Method = "POST";
        request.Credentials = new NetworkCredential(Username, Password);
        request.PreAuthenticate = true;
        if(!request.HaveResponse)
        {
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                Token = JsonUtil.ParseAsObject<JsonUserToken>(NetworkingUtil.GetStringFromHTTPResponse(response)).Token;
                Debug.Log(Token);
                return true;
            }
            Debug.LogError(response.StatusCode == HttpStatusCode.OK);
            return false;
        }
        else
        {
            return false;
        }
    }

    /// <summary>
    /// Sends an API Request via the given <see cref="RequestType"/> to the given url.
    /// If the request type includes sending data, an optional payload in json format can be added.
    /// Once the request ist successfull, the optional callback is invoked.
    /// </summary>
    /// <param name="type">HTTP Request Type</param>
    /// <param name="url">Complete url to which the equest should be send</param>
    /// <param name="payload">Optional Payload in json format</param>
    /// <param name="callback">Optional callback</param>
    public void SendAPIRequest(RequestType type, string url, string payload = null, ApiCallBack callback = null)
    {
        if (Token == "")
        {
            Debug.LogError("No token assigned. Can't make an API request.");
            return;
        }
        switch(type)
        {
            case RequestType.DELETE:
                StartCoroutine(Delete(url, callback));
                break;
            case RequestType.POST:
                StartCoroutine(Post(url, payload, callback));
                break;
            case RequestType.GET:
                StartCoroutine(Get(url, callback));
                break;
            case RequestType.PUT:
                StartCoroutine(Put(url, payload, callback));
                break;
        }
    }

    /// <summary>
    /// Unitywebrequest implementation to the post http method. Works async.
    /// This method shouldnt be called directly!
    /// </summary>
    /// <param name="url">URl to send</param>
    /// <param name="content">Payload content</param>
    /// <param name="callback">Callback after getting an answer</param>
    /// <returns>IEnumerator which handles internal data</returns>
    private IEnumerator Post(string url, string content, ApiCallBack callback = null)
    {
        var req = UnityWebRequest.Put(url, content);    // This trick formats the given content
        req.method = UnityWebRequest.kHttpVerbPOST;     // to bytes.
        req.SetRequestHeader("Authorization", "Bearer " + Token);
        req.SetRequestHeader("Content-Type", "application/json");
        req.SetRequestHeader("Accept", "application/json");
        yield return req.SendWebRequest();
        if(!req.isNetworkError && !req.isHttpError)
        {
            var RecentAPIResponse = new APIResponse(true, req.downloadHandler.text);
            if (callback != null)
            {
                callback.Invoke(RecentAPIResponse);
            }
        }
    }

    /// <summary>
    /// Unitywebrequest implementation to the put http method. Works async.
    /// This method shouldnt be called directly!
    /// </summary>
    /// <param name="url">URl to send</param>
    /// <param name="content">Payload content</param>
    /// <param name="callback">Callback after getting an answer</param>
    /// <returns>IEnumerator which handles internal data</returns>
    private IEnumerator Put(string url, string content, ApiCallBack callback = null)
    {
        var req = UnityWebRequest.Put(url, content);
        req.SetRequestHeader("Authorization", "Bearer " + Token);
        req.SetRequestHeader("Content-Type", "application/json");
        req.SetRequestHeader("Accept", "application/json");
        yield return req.SendWebRequest();
        if (!req.isNetworkError && !req.isHttpError)
        {
            var RecentAPIResponse = new APIResponse(true, req.downloadHandler.text);
            if (callback != null)
            {
                callback.Invoke(RecentAPIResponse);
            }
        }
    }

    /// <summary>
    /// Unitywebrequest implementation to the delete http method. Works async.
    /// This method shouldnt be called directly!
    /// </summary>
    /// <param name="url">URl to send</param>
    /// <param name="callback">Callback after getting an answer</param>
    /// <returns>IEnumerator which handles internal data</returns>
    private IEnumerator Delete(string url, ApiCallBack callback = null)
    {
        var req = UnityWebRequest.Delete(url);
        req.SetRequestHeader("Authorization", "Bearer " + Token);
        req.SetRequestHeader("Content-Type", "application/json");
        req.SetRequestHeader("Accept", "application/json");
        yield return req.SendWebRequest();
        if (!req.isNetworkError && !req.isHttpError)
        {
            var RecentAPIResponse = new APIResponse(true, req.downloadHandler.text);
            if (callback != null)
            {
                callback.Invoke(RecentAPIResponse);
            }
        }
    }

    /// <summary>
    /// Unitywebrequest implementation to the get http method. Works async.
    /// This method shouldnt be called directly!
    /// </summary>
    /// <param name="url">URl to send</param>
    /// <param name="callback">Callback after getting an answer</param>
    /// <returns>IEnumerator which handles internal data</returns>
    private IEnumerator Get(string url, ApiCallBack callback = null)
    {
        var req = UnityWebRequest.Get(url);
        req.SetRequestHeader("Authorization", "Bearer " + Token);
        req.SetRequestHeader("Content-Type", "application/json");
        req.SetRequestHeader("Accept", "application/json");
        yield return req.SendWebRequest();
        if (!req.isNetworkError && !req.isHttpError)
        {
            var RecentAPIResponse = new APIResponse(true, req.downloadHandler.text);
            if (callback != null)
            {
                callback.Invoke(RecentAPIResponse);
            }
        }
    }

    /// <summary>
    /// Unitywebrequest implementation foru the async authneticdation process.
    /// In its basics, its a extended copy of <see cref="Post(string, string, ApiCallBack)"/>, which gets additional credentials to handle the process.
    /// This method shouldnt be called directly!
    /// </summary>
    /// <param name="url">URl to send</param>
    /// <param name="username">Username for authentication</param>
    /// <param name="password">Password for authnetication</param>
    /// <param name="callback">Callback</param>
    /// <returns>IEnumerator which handles internal data</returns>
    private IEnumerator Authenticate(string url, string username, string password, ApiCallBack callback = null)
    {
        var req = UnityWebRequest.Post(url, "");
        req.SetRequestHeader("Authorization", "Basic " + System.Convert.ToBase64String(
        System.Text.Encoding.GetEncoding("UTF-8")
            .GetBytes(username + ":" + password)));
        yield return req.SendWebRequest();
        if (!req.isNetworkError && !req.isHttpError)
        {
            var RecentAPIResponse = new APIResponse(true, req.downloadHandler.text);
            Token = JsonUtil.ParseAsObject<JsonUserToken>(RecentAPIResponse.APIResponseContent).Token;
            if (callback != null)
            {
                callback.Invoke(RecentAPIResponse);
            }
        }
    }

    /// <summary>
    /// Sends an asnyc authentication request to the webserver.
    /// After recieving an answer, the optional callback will be invoked.
    /// </summary>
    /// <param name="callback">Optional callback, which will be invoked after recieving an answer.</param>
    public void LogIn(ApiCallBack callback = null)
    {
        StartCoroutine(Authenticate(Url + APIEndpoints.ENDPOINTTOKEN, Username, Password, callback));
    }

    /// <summary>
    /// Sends an async Post Request to "<see cref="Url"/>+<see cref="APIEndpoints.ENDPOINTTOKEN"/>" with the given payload as data.
    /// After recieving an answer, the optional callback will be invoked.
    /// </summary>
    /// <param name="payload">Post data, which should be sent, in json format.</param>
    /// <param name="callback">Optional callback, which will be invoked after recieving an answer.</param>
    public void SendToToken(string payload, ApiCallBack callback = null)
    {
        SendAPIRequest(RequestType.POST, Url + APIEndpoints.ENDPOINTTOKEN, payload, callback);
    }

    /// <summary>
    /// Sends an async Delete Request to "<see cref="Url"/>+<see cref="APIEndpoints.ENDPOINTTOKEN"/>".
    /// After recieving an answer, the optional callback will be invoked.
    /// </summary>
    /// <param name="callback">Optional callback, which will be invoked after recieving an answer.</param>
    public void DeleteFromToken(ApiCallBack callback = null)
    {
        SendAPIRequest(RequestType.DELETE, Url + APIEndpoints.ENDPOINTTOKEN, null, callback);
    }

    /// <summary>
    /// Sends an async Get Request to "<see cref="Url"/>+<see cref="APIEndpoints.ENDPOINTSESSION"/>".
    /// After recieving an answer, the optional callback will be invoked.
    /// </summary>
    /// <param name="callback">Optional callback, which will be invoked after recieving an answer.</param>
    public void RecieveoFromSession(ApiCallBack callback = null)
    {
        SendAPIRequest(RequestType.GET, Url + APIEndpoints.ENDPOINTSESSION, null, callback);
    }

    /// <summary>
    /// Sends an async Get Request to "<see cref="Url"/>+<see cref="APIEndpoints.ENDPOINTSESSIONPRVALUE"/>".
    /// After recieving an answer, the optional callback will be invoked.
    /// </summary>
    /// <param name="callback">Optional callback, which will be invoked after recieving an answer.</param>
    public void RecieveFromSessionPrValue(ApiCallBack callback = null)
    {
        SendAPIRequest(RequestType.GET, Url + APIEndpoints.ENDPOINTSESSIONPRVALUE, null, callback);
    }

    /// <summary>
    /// Sends an async Post Request to "<see cref="Url"/>+<see cref="APIEndpoints.EndpointDevice(devicename)"/>" with the given payload as data.
    /// After recieving an answer, the optional callback will be invoked.
    /// </summary>
    /// <param name="devicename">Endpoint of the device, which should recive the data (see <see cref="APIEndpoints.EndpointDevice(devicename)"/>)</param>
    /// <param name="payload">Post data, which should be sent, in json format.</param>
    /// <param name="callback">Optional callback, which will be invoked after recieving an answer.</param>
    public void SendToDevice(string devicename, string payload, ApiCallBack callback = null)
    {
        SendAPIRequest(RequestType.POST, Url + APIEndpoints.EndpointDevice(devicename), payload, callback);
    }

    /// <summary>
    /// Sends an async Get Request to "<see cref="Url"/>+<see cref="APIEndpoints.EndpointDevice(devicename)"/>".
    /// After recieving an answer, the optional callback will be invoked.
    /// </summary>
    /// <param name="devicename">Endpoint of the device, which should send the data (see <see cref="APIEndpoints.EndpointDevice(devicename)"/>)</param>
    /// <param name="callback">Optional callback, which will be invoked after recieving an answer.</param>
    public void RecieveFromDevice(string devicename, ApiCallBack callback = null)
    {
        SendAPIRequest(RequestType.GET, Url + APIEndpoints.EndpointDevice(devicename), null, callback);
    }

    /// <summary>
    /// Sends an async Post Request to "<see cref="Url"/>+<see cref="APIEndpoints.EndpointDeviceCalibrate(devicename)"/>" with the given payload as data.
    /// After recieving an answer, the optional callback will be invoked.
    /// </summary>
    /// <param name="devicename">Endpoint of the device, which should recive the data (see <see cref="APIEndpoints.EndpointDeviceCalibrate(devicename)"/>)</param>
    /// <param name="payload">Post data, which should be sent, in json format.</param>
    /// <param name="callback">Optional callback, which will be invoked after recieving an answer.</param>
    public void SendToDeviceCalibrate(string devicename, string payload, ApiCallBack callback = null)
    {
        SendAPIRequest(RequestType.POST, Url + APIEndpoints.EndpointDeviceCalibrate(devicename), payload, callback);
    }

    /// <summary>
    /// Sends an async Post Request to "<see cref="Url"/>+<see cref="APIEndpoints.EndpointDeviceSettings(devicename)"/>" with the given payload as data.
    /// After recieving an answer, the optional callback will be invoked.
    /// </summary>
    /// <param name="devicename">Endpoint of the device, which should recive the data (see <see cref="APIEndpoints.EndpointDeviceSettings(devicename)"/>)</param>
    /// <param name="payload">Post data, which should be sent, in json format.</param>
    /// <param name="callback">Optional callback, which will be invoked after recieving an answer.</param>
    public void SendToDeviceSettings(string devicename, string payload, ApiCallBack callback = null)
    {
        SendAPIRequest(RequestType.POST, Url + APIEndpoints.EndpointDeviceSettings(devicename), payload, callback);
    }

    /// <summary>
    /// Sends an async Get Request to "<see cref="Url"/>+<see cref="APIEndpoints.EndpointDeviceHistory(devicename, count)"/>".
    /// After recieving an answer, the optional callback will be invoked.
    /// </summary>
    /// <param name="devicename">Endpoint of the device, which should send the data (see <see cref="APIEndpoints.EndpointDeviceHistory(devicename, count)"/>)</param>
    /// <param name="count">Amount of values (see <see cref="APIEndpoints.EndpointDeviceHistory(devicename, count)"/>)</param>
    /// <param name="callback">Optional callback, which will be invoked after recieving an answer.</param>
    public void RecieveFromDeviceHistory(string devicename, int count, ApiCallBack callback = null)
    {
        SendAPIRequest(RequestType.GET, Url + APIEndpoints.EndpointDeviceHistory(devicename, count), null, callback);
    }
}

/// <summary>
/// Data class containing the server responses for http communication (mostly instanciated via <see cref="KinetosisServerApi.ApiCallBack(APIResponse response)"/>)
/// </summary>
public class APIResponse
{
    /// <summary>
    /// Indicates, if the original api call, which created this response, was succesfull.
    /// </summary>
    public bool Succeded { get;  private set; }

    /// <summary>
    /// Indicates, if the resulting data in <see cref="APIResponseContent"/> is in json format.
    /// </summary>
    public bool IsJson {
        get
        {
            //Maybe with newtonsoft to implement - todo
            return true;
        }
    }

    /// <summary>
    /// Storage for error content
    /// </summary>
    private string _errorContent;
    /// <summary>
    /// Storage for server response content, which doesnt contain any error.
    /// </summary>
    private string _apiResponseContent;

    /// <summary>
    /// Contains the answer from the webserver in the given format.
    /// If the original request resulted in an error, the content contains the error message.
    /// </summary>
    public string APIResponseContent { 
        get
        {
            if(Succeded)
            {
                return _apiResponseContent;
            }
            else
            {
                return _errorContent;
            }
        }
        private set
        {
            if (Succeded)
            {
                _apiResponseContent = value;
            }
            else
            {
                _errorContent = value;
            }
        }
    }

    /// <summary>
    /// Instanciates a new APIResponse with the given values.
    /// </summary>
    /// <param name="succeded">Indicator if the original api call was succesfull</param>
    /// <param name="response">Server response content</param>
    public APIResponse(bool succeded, string response)
    {
        Succeded = succeded;
        APIResponseContent = response;
    }
}

/// <summary>
/// Helper enumeration for different HTTP Methods.
/// </summary>
public enum RequestType
{
    GET,
    POST,
    PUT,
    DELETE
}

/// <summary>
/// Helper class for different url strings and api endpoints.
/// </summary>
public static class APIEndpoints
{
    /// <summary>
    /// String representation for localhost
    /// </summary>
    public const string URLLOCALHOST = "localhost";
    /// <summary>
    /// String representation for admin (username)
    /// </summary>
    public const string ADMINUSERNAME = "admin";
    /// <summary>
    /// String representation for admin (password)
    /// </summary>
    public const string ADMINPASSWORD = "admin";
    /// <summary>
    /// String representation for the token endpoint of the webserver
    /// </summary>
    public const string ENDPOINTTOKEN = "/token";
    /// <summary>
    /// String representation for the session endpoint of the webserver
    /// </summary>
    public const string ENDPOINTSESSION = "/session";
    /// <summary>
    /// String representation for the prvalue endpoint of the webserver
    /// </summary>
    public const string ENDPOINTSESSIONPRVALUE = "/session/prvalue";

    /// <summary>
    /// Retrieves the endpoint for the given device regarding trackingdata.
    /// Resulting string will be "/session/device/+devicename"
    /// </summary>
    /// <param name="devicename">Endpoint of the device</param>
    /// <returns>String representation for endpoint of the given device of the webserver</returns>
    public static string EndpointDevice(string deviceName)
    {
        return "/session/device/" + deviceName;
    }
    /// <summary>
    /// Retrieves the endpoint for the given device regarding calibrationdata.
    /// Resulting string will be "/session/device/+devicename+/calibrate"
    /// </summary>
    /// <param name="devicename">Endpoint of the device</param>
    /// <returns>String representation for endpoint of the given device of the webserver for calibration</returns>
    public static string EndpointDeviceCalibrate(string deviceName)
    {
        return "/session/device/" + deviceName + "/calibrate";
    }
    /// <summary>
    /// Retrieves the endpoint for the given device regarding settingsdata.
    /// Resulting string will be "/session/device/+devicename+/settings"
    /// </summary>
    /// <param name="devicename">Endpoint of the device</param>
    /// <returns>String representation for endpoint of the given device of the webserver for settings</returns>
    public static string EndpointDeviceSettings(string deviceName)
    {
        return "/session/device/" + deviceName + "/settings";
    }

    /// <summary>
    /// Retrieves the endpoint for the given device and amount fo values to get the requested data.
    /// Resulting string will be "/session/device/+devicename+/history/+count"
    /// </summary>
    /// <param name="devicename">Endpoint of the device</param>
    /// <param name="count">Amount fo values</param>
    /// <returns>String representation for endpoint of the given device and count regarding already recieved data.</returns>
    public static string EndpointDeviceHistory(string deviceName, int count)
    {
        return "/session/device/" + deviceName + "/history/" + count.ToString();
    }
}

