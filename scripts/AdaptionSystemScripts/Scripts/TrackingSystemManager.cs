﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

/// <summary>
/// This class controls and amanges every tracking system which is registered in the asset.
/// It provides also key listinings for the simulated trackers and holds all the tracking system results.
/// </summary>
public class TrackingSystemManager  {

	/// <summary>
	/// Dircionary for every tracking system in the system. Key is the signature of each system.
	/// </summary>
	private Dictionary<string, TrackingSystem> trackingSystems;
	/// <summary>
	/// List for every simulated system. Contains only references for each simulated tracking system.
	/// </summary>
	[HideInInspector]
	public List<STrackingSystem> simulatedTrackingSystems;
	/// <summary>
	/// Contains the currentyl selected simulated tracking system for which the key listinings apply
	/// </summary>
	[HideInInspector]
	public STrackingSystem activeTrackingSimulated;
	/// <summary>
	/// Current list position of the selected simulated trakcing system.
	/// </summary>
	private int listPos = 0;

	/// <summary>
	/// Contains all recieved values from each tracking system in a FIFO queue. Will be updated each frame.
	/// </summary>
	[HideInInspector]
	public Queue<TrackingSystemResult> RecentValues { get; private set; }

	/// <summary>
	/// Standard constructor. 
	/// For instanciating there should only be one TrackingSystemManager, which normally would lead towards a Singleton. 
	/// Singletons are mostly bad practice so it is possible to create more managers.
	/// </summary>
	public TrackingSystemManager()
    {
		trackingSystems = new Dictionary<string, TrackingSystem>();
		simulatedTrackingSystems = new List<STrackingSystem>();
		RecentValues = new Queue<TrackingSystemResult>();
	}

	/// <summary>
	/// Update routine to poll new tracking system results for each registered system.
	/// Reads out keypresses to determine current selected simulated tracking system and to manipulate its data.
	/// Gets a floating value to adjust each interval of the tracking systems.
	/// </summary>
	/// <param name="deltaTime">Passed time delta of the last frame</param>
	public void Update(float deltaTime)
    {
		//----
		//SimulatedTracker
		if (activeTrackingSimulated != null)
		{
			//Debug.Log(activeTrackingSimulated.Signature);
			if (Input.GetKeyDown(KeyCode.DownArrow))
			{
				listPos++;
				if (listPos >= simulatedTrackingSystems.Count)
				{
					listPos = 0;
				}
				activeTrackingSimulated = simulatedTrackingSystems[listPos];
				Debug.Log("Current selected simulated tracker: " + activeTrackingSimulated.Signature);
			}
			else if (Input.GetKeyDown(KeyCode.UpArrow))
			{
				listPos--;
				if (listPos < 0)
				{
					listPos = simulatedTrackingSystems.Count - 1;
				}
				activeTrackingSimulated = simulatedTrackingSystems[listPos];
				Debug.Log("Current selected simulated tracker: " + activeTrackingSimulated.Signature);
			}
			else if (Input.GetKeyDown(KeyCode.KeypadPlus))
			{
				activeTrackingSimulated.Increase();
				Debug.Log("Increasing value of tracker " + activeTrackingSimulated.Signature + " by " + activeTrackingSimulated.Steps);
			}
			else if (Input.GetKeyDown(KeyCode.KeypadMinus))
			{
				activeTrackingSimulated.Decrease();
				Debug.Log("Decreasing value of tracker " + activeTrackingSimulated.Signature + " by " + activeTrackingSimulated.Steps);
			}
		}
		else
        {
			if(simulatedTrackingSystems.Count > 0)
            {
				activeTrackingSimulated = simulatedTrackingSystems.FirstOrDefault();
            }
        }
		//----
		//Every Tracker
		Track(deltaTime);
	}

	/// <summary>
	/// Polls the results of every registered tracking system.
	/// Gets a floating value to adjust each interval of the tracking systems.
	/// </summary>
	/// <param name="deltaTime">Passed time delta of the last frame</param>
	private void Track(float deltaTime)
    {
		foreach (var trackingsystem in trackingSystems)
		{
			if (trackingsystem.Value.UpdateValue(deltaTime))
			{
				RecentValues.Enqueue(trackingsystem.Value.RecieveCurrentResult());
			}
		}
	}

	/// <summary>
	/// Standard callback after recieving settings.
	/// </summary>
	/// <param name="response">Response of the api</param>
	[Obsolete("Unused")]
	private void OnRecieve(APIResponse response)
    {

    }

	/// <summary>
	/// Sends the settings written in each tracking systems to the serrver. 
	/// Api must be given as parameter.
	/// An optional callback can be given, whcih is executed once the server answers.
	/// </summary>
	/// <param name="api">API of the server</param>
	/// <param name="callback">Optional callback</param>
	public void SendSettings(KinetosisServerAPI api, KinetosisServerAPI.ApiCallBack callback = null)
    {
		foreach(var trackingsystem in trackingSystems)
        {
			api.SendToDeviceSettings(trackingsystem.Value.Endpoint, trackingsystem.Value.Settings, callback);
        }
    }


	/// <summary>
	/// Sets all tracking systems to idle mode. 
	/// Ajn optional amounts value can be given. 
	/// For  -1 the duration will be infinite and the tracking systems must be set to another mode manually.
	/// For every other amount the standard callback in the tracking system will be executed to determine the next mode.
	/// </summary>
	/// <param name="forTicks"></param>
	public void SleepAll(int forTicks = -1)
    {
		foreach (var trackingsystem in trackingSystems)
		{
			trackingsystem.Value.SetMode(new TrackingSystemModeState(TrackingSystemMode.Idle, forTicks));
		}
	}

	/// <summary>
	/// Sets all tracking systems to callback mode. 
	/// Ajn optional amounts value can be given. 
	/// For  -1 the duration will be infinite and the tracking systems must be set to another mode manually.
	/// For every other amount the standard callback in the tracking system will be executed to determine the next mode.
	/// </summary>
	/// <param name="forTicks"></param>
	public void CalibrateAll(int forTicks = -1)
	{
		foreach (var trackingsystem in trackingSystems)
		{
			trackingsystem.Value.SetMode(new TrackingSystemModeState(TrackingSystemMode.Idle, forTicks));
		}
	}

	/// <summary>
	/// Sets all tracking systems to tracking mode. 
	/// Ajn optional amounts value can be given. 
	/// For  -1 the duration will be infinite and the tracking systems must be set to another mode manually.
	/// For every other amount the standard callback in the tracking system will be executed to determine the next mode.
	/// </summary>
	/// <param name="forTicks"></param>
	public void TrackAll(int forTicks = -1)
	{
		foreach (var trackingsystem in trackingSystems)
		{
			trackingsystem.Value.SetMode(new TrackingSystemModeState(TrackingSystemMode.Idle, forTicks));
		}
	}

	/// <summary>
	/// Gets a string (signatures) collections of all registered tracking systems.
	/// </summary>
	/// <returns>Signature collections of all registered tracking systems.</returns>
	public IEnumerable<string> GetActiveTrackingSystems()
	{
		return trackingSystems.Select(x => x.Key);
	}

	/// <summary>
	/// Adds the given tracking system to the system.
	/// </summary>
	/// <typeparam name="T">Tracking System Type</typeparam>
	/// <param name="trackingSystem">Tracking System to add</param>
	/// <returns>Success/Failure</returns>
	public bool AddTrackingSystem<T>(T trackingSystem) where T: TrackingSystem
	{
		if(trackingSystems.ContainsKey(trackingSystem.Signature))
        {
			return false;
        }
		if(trackingSystem is STrackingSystem)
        {
			simulatedTrackingSystems.Add(trackingSystem as STrackingSystem);
        }
		trackingSystems.Add(trackingSystem.Signature, trackingSystem);
		return true;
	}

	/// <summary>
	/// Removes the given tracking system from the system.
	/// </summary>
	/// <typeparam name="T">Tracking System Type</typeparam>
	/// <param name="trackingSystem">Tracking System to remove</param>
	/// <returns>Success/Failure</returns>
	public bool RemoveTrackingSystem<T>(T trackingSystem) where T : TrackingSystem
	{
		if (!trackingSystems.ContainsKey(trackingSystem.Signature))
		{
			return false;
		}
		if (trackingSystem is STrackingSystem)
		{
			listPos--;
			simulatedTrackingSystems.Remove(trackingSystem as STrackingSystem);
			if (simulatedTrackingSystems.Count <= 0)
			{
				listPos = 0;
				activeTrackingSimulated = null;
			}
			else
            {
				if (listPos < 0)
				{
					listPos = 0;
				}
				activeTrackingSimulated = simulatedTrackingSystems.ElementAt(listPos);
			}
		}
		trackingSystems.Remove(trackingSystem.Signature);
		return true;
	}

	/// <summary>
	/// Returns the tracking system with the given signature.
	/// Return vlaue will be the base class.
	/// If no tracking system is found, null will be returned.
	/// </summary>
	/// <param name="signature">Signature of the tracking system</param>
	/// <returns>Requested tracking system as base class</returns>
	public TrackingSystem GetTrackingSystemBySignature(string signature)
	{
		if (trackingSystems.ContainsKey(signature))
		{
			return trackingSystems[signature];
		}
		return null;
	}

	/// <summary>
	/// Returns the tracking system with the reference system.
	/// Return vlaue will be the base class.
	/// If no tracking system is found, null will be returned.
	/// </summary>
	/// <typeparam name="T">Type of the tracking system</typeparam>
	/// <param name="equalSystem">Reference system</param>
	/// <returns>Requested tracking system as base class</returns>
	public TrackingSystem GetITrackingSystem<T>(T equalSystem) where T : TrackingSystem
	{
		if (trackingSystems.ContainsKey(equalSystem.Signature))
		{
			return trackingSystems[equalSystem.Signature];
		}
		return null;
	}

	/// <summary>
	/// Returns the tracking system with the given signature.
	/// If no tracking system is found, null will be returned.
	/// </summary>
	/// <typeparam name="T">Type of the tracking system</typeparam>
	/// <param name="signature">Signature of the tracking system</param>
	/// <returns>Requested tracking system</returns>
	public T GetTrackingSystemBySignatureConverted<T>(string signature) where T : TrackingSystem
	{
		if (trackingSystems.ContainsKey(signature))
		{
			return (T)trackingSystems[signature];
		}
		return default(T);
	}

	/// <summary>
	/// Returns the tracking system with the reference system
	/// If no tracking system is found, null will be returned.
	/// </summary>
	/// <typeparam name="T">Type of the tracking system</typeparam>
	/// <param name="equalSystem">Signature of the tracking system</param>
	/// <returns>Requested tracking system</returns>
	public T GetITrackingSystemConverted<T>(T equalSystem) where T : TrackingSystem
	{
		if (trackingSystems.ContainsKey(equalSystem.Signature))
		{
			return (T) trackingSystems[equalSystem.Signature];
		}
		return null;
	}
}

/// <summary>
/// Data class for results of each tracking system.
/// </summary>
public class TrackingSystemResult
{
	/// <summary>
	/// Contains the data formated as json string
	/// </summary>
	[HideInInspector]
	public string ValueAsJson;
	/// <summary>
	/// Contains the signature string of the parent tracking system
	/// </summary>
	[HideInInspector]
	public string Owner;
	/// <summary>
	/// Contains the mode of the parent system
	/// </summary>
	[HideInInspector]
	public TrackingSystemMode Mode;

	/// <summary>
	/// Creates a new tracking system result with the given data
	/// </summary>
	/// <param name="valueAsJson">Data as json</param>
	/// <param name="owner">Parent tracking system</param>
	/// <param name="mode">Mode of the tracking system</param>
	public TrackingSystemResult(string valueAsJson, string owner, TrackingSystemMode mode)
    {
		ValueAsJson = valueAsJson;
		Owner = owner;
		Mode = mode;

	}
}

