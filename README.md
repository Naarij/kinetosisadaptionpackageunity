# Unity Kinetosis Adaption System Asset

## What is this about?

The "Unity Kinetosis Adaption System Asset" is a package for Unity 2018.2.5f1 (may work for other versions aswell, specifly tested in this version), which works with the Kinetosis-Server (https://gitlab.com/Naarij/kinetosis-detector (fork of https://gitlab.com/SamuelBlickle/kinetosis-detector)) to predict kinetosis in a VR-Application and to enable adaption possibilities. <br>
To predict kinetosis, the asset and server works with different sensor systems, which can be attached to the asset or directly to the server. The Asset can then be imported into an Unity VR-Application and be configured to behave differently, when a certain threshold of the given PRValue (indicating kinetosis) is reached. Those values can be configured in the Asset, aswell on the server. <br>
It was developed as a bachelor thesis at the University of Applied Sciences Aalen and will be maintained by more contributors throughout different projects.

## Requires

To install and use the Asset, there are some remarks and prerequisits:

- Supported Unity Version
- SteamVR Asset
- SteamVR Runtime
- Vive SR.Anipal Asset
- Vive SR.Anipal Runtime
- TobiiXR Eyetracking Asset (is needed for debugging and should be installed - can be removed in later versions)
- **NEW:** JSON.Net Parsing Asset (is needed for parsing json and should be installed - can be removed in later versions)
- HTC Vive Pro Eye
- Windows 10 (not tested on other OS)

**For a remote server of the kinetosis detector (https://gitlab.com/SamuelBlickle/kinetosis-detector) a stable internet connection is required.**

All Assets can be retrieved via the Unity Asset Store or the homepages.

## Install

If you want to use the asset in a VR-Application inside Unity, follow the steps below to install it:

1. Make sure, SteamVR is added as an Asset in your application and the runtime also works. This should normally be the case.
2. Download the Vive SR.Anipal Asset and Runtime: https://developer.vive.com/resources/vive-sense/sdk/vive-eye-tracking-sdk-sranipal/
3. Install the runtime and execute it. A little robot with orange eyes should show up in your right icon bar. If the robot eye are blacked out, replug your HTC Vive and restart SteamVR and the Vive Runtime.
4. Import the Vive SR.Aniapl SDK in your unity project.
5. Download the TobiiXR EyeTracking SDK for VR. On their website you finde the same walkthrough for the installation steps above: https://vr.tobii.com/sdk/develop/unity/getting-started/vive-pro-eye/
6. Import the TobiiXR EyeTracking SDK to your Unity Project.
7. Import the JSON.Net Asset to your Unity Project: https://assetstore.unity.com/packages/tools/input-management/json-net-for-unity-11347
8. Import the Kinetosis Adaption System Asset from this repo to your project.
9. Your done. Enjoy testing and configuring this asset :)

## How to use

If you want to know, how this asset works, check out the Usage.txt file.
