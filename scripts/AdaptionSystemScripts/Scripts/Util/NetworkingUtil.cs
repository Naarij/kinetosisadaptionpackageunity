﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using Newtonsoft.Json;

[Obsolete("Old synchronous parsing functions for web communication . shouldnt be used anymore")]
public class NetworkingUtil {

    public static string GetStringFromHTTPResponse(HttpWebResponse response)
    {
        string result = "";
        using (Stream stream = response.GetResponseStream())
        {
            StreamReader reader = new StreamReader(stream, Encoding.UTF8);
            result = reader.ReadToEnd();
        }
        return result;
    }

    public static void SendPayloadViaRequestStream(HttpWebRequest request, string payloadAsJson)
    {
        request.ContentType = "application/json";

        var data = Encoding.ASCII.GetBytes(payloadAsJson);

        request.ContentLength = data.Length;

        using (Stream stream = request.GetRequestStream())
        {
            stream.Write(data, 0 , data.Length);
        }
    }

}
