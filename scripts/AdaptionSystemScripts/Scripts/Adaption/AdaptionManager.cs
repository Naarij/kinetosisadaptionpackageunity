﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Controls and manages every interaction regarding pr comparison and threshold.
/// It holds and invokes callbacks for exceeding and deceeding the threshold.
/// </summary>
public class AdaptionManager {

    /// <summary>
    /// Interval in ms, in which the adaption manager pulls the new pr value and compares it with the threshold
    /// </summary>
    public int Interval { get; private set; }
    /// <summary>
    /// Current PrValue, which will be compared with the threshold
    /// </summary>
    public float PrValue { get; set; }
    /// <summary>
    /// Contains session information regarding interaction shortcuts, threshold and registered devices
    /// </summary>
    public JsonSessionInfo Info { get; set; }

    /// <summary>
    /// Rest interval in ms which builds up a timedelta to ensure comparison in a specified interval
    /// </summary>
    private int restInterval;

    /// <summary>
    /// Reference to server api to make requests
    /// </summary>
    private KinetosisServerAPI Api;

    /// <summary>
    /// Function signature which handles callbacks regarding exceeding and deceeding the threshold.
    /// </summary>
    /// <param name="currentValue">Current Pr Value</param>
    /// <param name="suggestion"><see cref="APIInteraction"/> which holds all interaction shortcuts</param>
    public delegate void OnThreshold(float currentValue, APIInteraction suggestion);
    /// <summary>
    /// Callback function for exceeding the threshold
    /// </summary>
    public OnThreshold CallbackReached { get; set; }
    /// <summary>
    /// Callback function for deceeding the threshold
    /// </summary>
    public OnThreshold CallbackBack { get; set; }

    /// <summary>
    /// Current State of the prvalue to prevent multiple invokes of the callback functions
    /// </summary>
    private PrState CurrentState;

    /// <summary>
    /// Instanciates an Adaption manager with the given values. 
    /// </summary>
    /// <param name="interval">Interval in ms, in which updating and comparison should take place</param>
    /// <param name="api">Reference of the server api</param>
    /// <param name="callbackReached">Callback for exceeding</param>
    /// <param name="callbackBack">Callback for deceeding</param>
    public AdaptionManager(int interval, KinetosisServerAPI api, OnThreshold callbackReached, OnThreshold callbackBack)
    {
        Interval = interval;
        restInterval = interval;
        Api = api;
        PrValue = 0;
        CallbackReached = callbackReached;
        CallbackBack = callbackBack;
        CurrentState = PrState.Lower;
    }

    /// <summary>
    /// Updates the manager. 
    /// The given param is required to check the interval.
    /// </summary>
    /// <param name="deltaTime">Time delta of the previous frame</param>
    public void Update(float deltaTime)
    {
        if(Info == null)
        {
            return;
        }
        restInterval -= (int)(deltaTime * 1000);
        if(restInterval <= 0)
        {
            restInterval = Interval;
            Api.RecieveFromSessionPrValue(OnPrRecieve);
            var updatedState = GetState(PrValue, Info.Threshold);
            switch (CurrentState)
            {
                case PrState.Lower:
                {
                    if(updatedState != PrState.Lower)
                    {
                        CallbackReached.Invoke(PrValue, new APIInteraction(Info));
                    }
                    break;
                }
                case PrState.Higher:
                {
                    if (updatedState != PrState.Higher)
                    {
                        CallbackBack.Invoke(PrValue, new APIInteraction(Info));
                    }
                    break;
                }
            }
            CurrentState = updatedState;
        }
    }

    /// <summary>
    /// Checks the state of the given floating values.
    /// If equal or higher, PrState.Higher will be returned.
    /// </summary>
    /// <param name="current">Current Pr Value</param>
    /// <param name="threshold">TThreshold for comparison</param>
    /// <return>Pr State for the given floating values</return>
    private PrState GetState(float current, float threshold)
    {
        if (current < threshold)
            return PrState.Lower;
        else
            return PrState.Higher;
    }

    /// <summary>
    /// Callback for overwriting the current PRValue with the updated value from the api.
    /// </summary>
    /// <param name="response">Server response</param>
    private void OnPrRecieve(APIResponse response)
    {
        if(response.Succeded)
        {
            if(response.IsJson)
            {
                PrValue = JsonUtil.ParseAsObject<JsonPrValue>(response.APIResponseContent).current_pr_value;
            }
        }
        else
        {
            //Do nothing
        }
    }
}

/// <summary>
/// Data class, which gets the current session info and retrieves all interaction shortcuts from it.
/// Those will be rearranged to a list containing each interaction shortcut.
/// </summary>
public class APIInteraction
{
    /// <summary>
    /// Storage for each interaction shortcut string
    /// </summary>
    public List<string> InteractionSuggestions { get; set; }

    /// <summary>
    /// Instanciates a new APIInteraction data class with the given session info.
    /// The interaction shortcuts will be written in a list.
    /// </summary>
    /// <param name="info">Session info containing different interaction shortcuts</param>
    public APIInteraction(JsonSessionInfo info)
    {
        InteractionSuggestions = new List<string>();
        foreach(var element in info.Session_Interaction_Shortcut)
        {
            InteractionSuggestions.Add(element);
        }
    }
}

/// <summary>
/// Helper to represent the result of the comparison between the current PrValue and the threshold.
/// </summary>
public enum PrState
{
    Lower = 0,
    Higher = 1
}