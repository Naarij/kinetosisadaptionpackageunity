﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Sensor system for the simulated skin salvation tracker.
/// Retrieves values about the current value as floats.
/// </summary>
public class SSkinSalvationTrackingSystem : STrackingSystem 
{
    /// <summary>
    /// Rest interval to calculate the current time delta
    /// </summary>
    private int restInterval;

    /// <summary>
    /// Storage for the current value.
    /// </summary>
    private float currentValue;
    /// <summary>
    /// Storage for the current origin value for random generation.
    /// </summary>
    private float origin;

    /// <summary>
    /// Storage for the range of random generation.
    /// </summary>
    private float range;

    /// <summary>
    /// Instanciates a new simulated skin salvation tracker with the given values.
    /// Calls <see cref="TrackingSystem(string endpoint, int interval, OnFinsihAmount callback = null, int amountValues = -1, string settings = null)"/> with the given values.
    /// Needs to get an unique signature when implemented
    /// </summary>
    /// <param name="endpoint">Endpoint of the sensor on server side</param>
    /// <param name="interval">Interval in ms, in which the sensor updates its values</param>
    /// <param name="callback">Callback method to determine next state</param>
    /// <param name="amountValues">Amount of values, after which the state should change</param>
    /// <param name="settings">Settings which this sensor should overwrite on the server</param>
    public SSkinSalvationTrackingSystem(string endpoint, int interval, OnFinsihAmount callback = null, int amountValues = -1, string settings = null)
        : base(endpoint, interval, callback, amountValues, settings)
    {
        Signature = typeof(SSkinSalvationTrackingSystem).Name;
        restInterval = interval;

        IsGeneratingRandom = false;

        Steps = 0.1f;
        currentValue = 0.1f;
        origin = currentValue;
        range = 0f;
    }

    /// <summary>
    /// Sets the steps to the new value.
    /// </summary>
    /// <param name="steps">New Value of the steps</param>
    public override void SetSteps(float steps)
    {
        Steps = steps;
    }

    /// <summary>
    /// Increases the current value by <see cref="Steps"/>
    /// </summary>
    /// <return>Success/Failure</return>
    public override bool Increase()
    {
        currentValue += Steps;
        origin = currentValue;
        return true;
    }

    /// <summary>
    /// Decreases the current value by <see cref="Steps"/>
    /// </summary>
    /// <return>Success/Failure</return>
    public override bool Decrease()
    {
        currentValue -= Steps;
        origin = currentValue;
        return true;
    }

    /// <summary>
    /// Sets the random generation lever.
    /// If true, a range can be given for random generation around the original value.
    /// </summary>
    /// <param name="enable">Enable/Disable random generation</param>
    /// <param name="range">Range for random generation</param>
    /// <return>Success/Failure</return>
    public override bool RandomGeneration(bool enable, float range = 0)
    {
        if (!enable)
        {
            IsGeneratingRandom = false;
        }
        else
        {
            if (range < 0)
            {
                return false;
            }
            this.range = range;
            IsGeneratingRandom = true;
        }
        return true;
    }

    /// <summary>
    /// Implemented function from base class to overwrite mode. 
    /// Should normally recieve the result of the callback as param. 
    /// </summary>
    /// <param name="mode">New mode with amount of values</param>
    /// <returns>Success/Failure</returns>
    public override bool SetMode(TrackingSystemModeState mode)
    {
        Mode = mode.Mode;
        if (mode.AmountValues < 0 && mode.AmountValues != -1)
        {
            Debug.LogWarning(mode.AmountValues.ToString() + " is not a valid argument for tracking system " + Endpoint + "! Changing to -1. Adjust manually.");
            this.AmountValues = -1;
            return false;
        }
        AmountValues = mode.AmountValues;
        return true;
    }

    /// <summary>
    /// Updates the tracking system in the given interval.
    /// Returns true, if a new value is recorded.
    /// </summary>
    /// <param name="deltaTime">TimeDelta to the previous frame</param>
    /// <returns>True if new value, else false</returns>
    public override bool UpdateValue(float deltaTime)
    {
        if (!CheckInterval(deltaTime))
        {
            return false;
        }
        if (!CheckAmount())
        {
            return false;
        }
        if (Mode != TrackingSystemMode.Idle)
        {
            CollectData();
            return true;
        }
        return false;
    }

    /// <summary>
    /// Checks if the amount of values is overstept and the mode should be changed.
    /// Also, once called, decrements the current amount of values.
    /// If the current amount of values is 0, the callback method to determine the new mod is invoked.
    /// </summary>
    /// <returns>True if amount is not 0, else false</returns>
    private bool CheckAmount()
    {
        if (AmountValues == 0)
        {
            if (!SetMode(Callback.Invoke(Mode)))
            {
                Mode = TrackingSystemMode.Idle;
                Debug.LogWarning("Callback to change mode failed - maybe cause of invalid arguments. Tracker " + Endpoint + "now in idle.");
            }
            return false;
        }
        else
        {
            if (AmountValues != -1)
            {
                AmountValues--;
            }
            return true;
        }
    }

    /// <summary>
    /// Checks if the system can be updated in the current interval.
    /// Also, once called, decrements the current restinterval by the given deltatime.
    /// If the current restinterval is below or equal 0, true will be returned and the interval is reset.
    /// </summary>
    /// <param name="deltaTime">DeltaTime of the previous frame</param>
    /// <returns>True if restinterval is below or equal 0, else false</returns>
    private bool CheckInterval(float deltaTime)
    {
        restInterval -= (int)(deltaTime * 1000);
        if (restInterval > 0)
        {
            return false;
        }
        else
        {
            restInterval = Interval - Math.Abs(restInterval);
            return true;
        }
    }

    /// <summary>
    /// Helper method to retrive data.
    /// </summary>
    private void CollectData()
    {
        if (IsGeneratingRandom)
        {
            currentValue = UnityEngine.Random.Range(origin - range, origin + range);
        }
        CurrentResult = new TrackingSystemResult(JsonUtil.ParseAsJson(new JsonFloat(currentValue)), Endpoint, Mode);
    }
}
