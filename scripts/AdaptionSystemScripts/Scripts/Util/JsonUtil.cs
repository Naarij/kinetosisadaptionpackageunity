﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Principal;
using UnityEngine;
using Newtonsoft;
using Newtonsoft.Json.Linq;
using System.Linq;
using Newtonsoft.Json.Serialization;
using System.Reflection;
using Newtonsoft.Json;

/// <summary>
/// Helper class for different json operations and strings
/// </summary>
public class JsonUtil 
{
    /// <summary>
    /// Retrieves an empty json string
    /// </summary>
    public static string JSONEMPTY
    {
        get
        {
            return "{}";
        }
    }

    /// <summary>
    /// Parses the given object as json string.
    /// </summary>
    /// <typeparam name="T">Type of the object to parse</typeparam>
	/// <param name="objectToParse">Object, which should be parsed</param>
	/// <returns>Correspondenting json string</returns>
    public static string ParseAsJson<T>(T objectToParse)
    {
        return JsonConvert.SerializeObject(objectToParse);
    }

    /// <summary>
    /// Parses the given object as json string.
    /// Uses the given contract resolver.
    /// </summary>
    /// <typeparam name="T">Type of the object to parse</typeparam>
	/// <param name="objectToParse">Object, which should be parsed</param>
    /// <param name="defaultContractResolver">Resolver for parsing</param>
	/// <returns>Correspondenting json string</returns>
    [Obsolete]
    public static string ParseAsJson<T>(T objectToParse, DefaultContractResolver defaultContractResolver)
    {
        return JsonConvert.SerializeObject(objectToParse, new JsonSerializerSettings() { ContractResolver = defaultContractResolver });
    }

    /// <summary>
    /// Parses the given object as json string and encapsulates it into an json object with key "value".
    /// </summary>
    /// <typeparam name="T">Type of the object to parse</typeparam>
	/// <param name="objectToParse">Object, which should be parsed</param>
	/// <returns>Correspondenting json string</returns>
    public static string ParseAsJsonObject<T>(T objectToParse)
    {
        var value = JObject.FromObject(objectToParse);
        JProperty p = new JProperty("value", value);
        var result = new JObject();
        result.Add(p);
        return result.ToString();
    }

    /// <summary>
    /// Adds the given property name to the given json string as key with the given value.
    /// Parses the value to json.
    /// </summary>
    /// <typeparam name="T">Type of the object to parse</typeparam>
    /// <param name="json">Json string which should be modified</param>
    /// <param name="propertyName">Key of the added property</param>
	/// <param name="propertyValue">Object, which should be parsed</param>
	/// <returns>Correspondenting json string</returns>
    public static string AddPropertyToJsonString<T>(string json, string propertyName, T propertyValue)
    {
        var value = JObject.Parse(json);
        value.Add(new JProperty(propertyName, JToken.FromObject(propertyValue)));
        return value.ToString();
    }

    /// <summary>
    /// Renames the given property name in the given json string to the given key.
    /// </summary>
    /// <param name="json">Json string which should be modified</param>
    /// <param name="propertyName">Key of the correspondenting property</param>
	/// <param name="desiredName">New name</param>
	/// <returns>Correspondenting json string</returns>
    public static string RenamePropertyInJsonString(string json, string propertyName, string desiredName)
    {
        var value = JObject.Parse(json);
        var propertyToChange = value.Properties().FirstOrDefault(x => x.Name.Equals(propertyName));
        if(propertyToChange == null)
        {
            Debug.Log("No property found.");
            return json;
        }
        JProperty replace = new JProperty(desiredName, propertyToChange.Value);
        value.Remove(propertyToChange.Name);
        value.AddFirst(replace);
        return value.ToString();
    }

    /// <summary>
    /// Parses the given json string as object.
    /// </summary>
    /// <typeparam name="T">Type of the object to retrieve</typeparam>
	/// <param name="jsonString">Json string representing the object</param>
	/// <returns>Correspondenting object</returns>
    public static T ParseAsObject<T>(string jsonString)
    {
        return Newtonsoft.Json.JsonConvert.DeserializeObject<T>(jsonString);
    }

    /// <summary>
    /// Verifies the given string as valid json string
    /// </summary>
	/// <param name="jsonString">String to verify</param>
	/// <returns>True if valid json, else false</returns>
    public static bool VerifyJsonString(string jsonString)
    {
        if(jsonString == "")
        {
            return false;
        }
        if(jsonString.Trim().StartsWith("{") && jsonString.Trim().EndsWith("}"))
        {
            try
            {
                JToken tok = JToken.Parse(jsonString);
                Debug.Assert(!tok.ToString().Equals(jsonString));
            }
            catch (Exception e)
            {
                Debug.LogError(e.StackTrace);
                return false;
            }
            return true;
        }
        else
        {
            return false;
        }
    }
}

[Obsolete]
public class YawPitchRollContractResolver : DefaultContractResolver
{
    public static readonly YawPitchRollContractResolver instance = new YawPitchRollContractResolver();

    protected override JsonProperty CreateProperty(MemberInfo member, MemberSerialization memberSerialization)
    {
        var resultingProperty = base.CreateProperty(member, memberSerialization);
        if(resultingProperty.DeclaringType != typeof(Vector3))
        {
            return resultingProperty;
        }
        if(resultingProperty.PropertyName.ToLower().Equals("x"))
        {
            resultingProperty.PropertyName = "roll";
        }
        else if (resultingProperty.PropertyName.ToLower().Equals("y"))
        {
            resultingProperty.PropertyName = "pitch";
        }
        else if (resultingProperty.PropertyName.ToLower().Equals("z"))
        {
            resultingProperty.PropertyName = "yaw";
        }
        return resultingProperty;
    }
}
