﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Base class for every sensor system.
/// Provides different values and interfaces to control different kinds of sensors.
/// </summary>
public abstract class TrackingSystem
{
    /// <summary>
    /// Endpoint of the sensor on server side.
    /// Value should be the endpoint only, so it matches <see cref="KinetosisServerApi.SendToDevice(string devicename, string payload, ApiCallBack callback = null)"/>
    /// </summary>
    public virtual string Endpoint
    {
        get;
        protected set;
    }
    /// <summary>
    /// Unique signature of the sensor which is used to identify the sensor in the <see cref="TrackingSystemManager"/>.
    /// </summary>
    public virtual string Signature
    {
        get;
        protected set;
    }

    /// <summary>
    /// Storage for the settings string which wants to overwrite the settings on ther server
    /// </summary>
    public virtual string Settings
    {
        get;
        protected set;
    }

    /// <summary>
    /// Interval in ms, in which the sensor pulls new values.
    /// </summary>
    public virtual int Interval
    {
        get;
        protected set;
    }

    /// <summary>
    /// Amount of values, after which the sensor should change its mode.
    /// If 0 is reached the <see cref="Callback"/> gets invoked and is given to <see cref="SetMode(TrackingSystemModeState newMode)"/> to get the next mode.
    /// For infinite values or manually mode changes, set to -1.
    /// </summary>
    public virtual int AmountValues
    {
        get;
        protected set;
    }

    /// <summary>
    /// Current mode of the tracking system.
    /// </summary>
    public virtual TrackingSystemMode Mode
    {
        get;
        protected set;
    }

    /// <summary>
    /// Callback method, which gets invoked when <see cref="AmountValues"/> is 0.
    /// </summary>
    protected OnFinsihAmount Callback
    {
        get;
        set;
    }

    /// <summary>
    /// Contains the current updated sensor data of the underlying tracking system.
    /// </summary>
    protected virtual TrackingSystemResult CurrentResult
    {
        get;
        set;
    }

    /// <summary>
    /// Function signature for the <see cref="Callback"/> function to determine the next sensor mode.
    /// Must never return null!
    /// </summary>
    /// <param name="from">Original mode</param>
    /// <return>Next mode</return>
    public delegate TrackingSystemModeState OnFinsihAmount(TrackingSystemMode from);

    /// <summary>
    /// Instanciates a new tracking system with the given values.
    /// Needs to get an unique signature when implemented
    /// </summary>
    /// <param name="endpoint">Endpoint of the sensor on server side</param>
    /// <param name="interval">Interval in ms, in which the sensor updates its values</param>
    /// <param name="callback">Callback method to determine next state</param>
    /// <param name="amountValues">Amount of values, after which the state should change</param>
    /// <param name="settings">Settings which this sensor should overwrite on the server</param>
    public TrackingSystem(string endpoint, int interval, OnFinsihAmount callback = null, int amountValues = -1, string settings = null)
    {
        Signature = typeof(TrackingSystem).Name;
        Endpoint = endpoint;
        Interval = interval;
        Callback = callback;
        AmountValues = amountValues;
        Settings = settings;

        Mode = TrackingSystemMode.Idle; //Standard mode

        CurrentResult = default(TrackingSystemResult);

        if (Callback == null)
        {
            Callback = StandardOnFinish;
        }
        if (AmountValues > 0)
        {
            Mode = TrackingSystemMode.Calibrating;
        }
    }

    /// <summary>
    /// Returns the current sensor data.
    /// </summary>
    /// <return>Sensordata with original mode and owner</return>
    public virtual TrackingSystemResult RecieveCurrentResult()
    {
        return CurrentResult;
    }

    /// <summary>
    /// Sets the interval to the new value.
    /// </summary>
    /// <param name="newValue">New Value of the interval</param>
    public virtual void SetInterval(int newValue)
    {
        Interval = Interval;
    }

    /// <summary>
    /// Sets the endpoint to the new value.
    /// </summary>
    /// <param name="endpoint">New Value of the endpoint</param>
    public virtual void SetEndpoint(string endpoint)
    {
        Endpoint = endpoint;
    }

    /// <summary>
    /// Sets the settings to the new value.
    /// </summary>
    /// <param name="settings">New Value of the settings</param>
    public virtual void SetSettings(string settings)
    {
        Settings = settings;
    }

    /// <summary>
    /// Sets the callback to the new value.
    /// </summary>
    /// <param name="callback">New Value of the callback</param>
    public virtual void SetCallback(OnFinsihAmount callback)
    {
        if (callback == null)
        {
            return;
        }
        Callback = callback;
    }

    /// <summary>
    /// Standard <see cref="Callback"/> function, if its not overwritten by the constructor.
    /// </summary>
    /// <param name="from">Original mode</param>
    /// <returns>Next mode</returns>
    protected TrackingSystemModeState StandardOnFinish(TrackingSystemMode from)
    {
        return new TrackingSystemModeState()
        {
            AmountValues = -1,
            Mode = TrackingSystemMode.Idle
        };
    }

    /// <summary>
    /// Update routine of the tracking system. 
    /// Should pull its values in the given interval.
    /// Returns true, if a new value is present.
    /// </summary>
    /// <param name="deltaTime">DeltaTime to the previous frame</param>
    /// <returns>True if new value is pulled, esle false.</returns>
    public abstract bool UpdateValue(float deltaTime);

    /// <summary>
    /// Last check before setting mode. 
    /// Should normally recieve the result of the callback as param. 
    /// </summary>
    /// <param name="mode">New mode with amount of values</param>
    /// <returns>Success/Failure</returns>
    public abstract bool SetMode(TrackingSystemModeState newMode);
}

/// <summary>
/// Base class for every simulated sensor system.
/// Provides different values and interfaces to control manipulate different kinds of sensors.
/// </summary>
public abstract class STrackingSystem : TrackingSystem
{
    /// <summary>
    /// Steps for numerical values, in which they should be manipulated when a key is pressed.
    /// </summary>
    public virtual float Steps
    {
        get;
        protected set;
    }

    /// <summary>
    /// Lever to set the sensor to random generation mode.
    /// </summary>
    public virtual bool IsGeneratingRandom
    {
        get;
        protected set;
    }
    /// <summary>
    /// Instanciates a new simulated tracking system with the given values.
    /// Needs to get an unique signature when implemented
    /// </summary>
    /// <param name="endpoint">Endpoint of the sensor on server side</param>
    /// <param name="interval">Interval in ms, in which the sensor updates its values</param>
    /// <param name="callback">Callback method to determine next state</param>
    /// <param name="amountValues">Amount of values, after which the state should change</param>
    /// <param name="settings">Settings which this sensor should overwrite on the server</param>
    public STrackingSystem(string endpoint, int interval, OnFinsihAmount callback = null, int amountValues = -1, string settings = null)
        : base(endpoint, interval, callback, amountValues, settings)
    {

    }

    /// <summary>
    /// Sets the steps to the new value.
    /// </summary>
    /// <param name="steps">New Value of the steps</param>
    public abstract void SetSteps(float steps);

    /// <summary>
    /// Increases the current value by <see cref="Steps"/>
    /// Needs to be implemented to get adjusted for non numerical values.
    /// </summary>
    /// <return>Success/Failure</return>
    public abstract bool Increase();
    /// <summary>
    /// Decreases the current value by <see cref="Steps"/>
    /// Needs to be implemented to get adjusted for non numerical values.
    /// </summary>
    /// <return>Success/Failure</return>
    public abstract bool Decrease();

    /// <summary>
    /// Sets the random generation lever.
    /// If true, a range can be given for random generation around the original value.
    /// </summary>
    /// <param name="enable">Enable/Disable random generation</param>
    /// <param name="range">Range for random generation</param>
    /// <return>Success/Failure</return>
    public abstract bool RandomGeneration(bool enable, float range = 0);
}

/// <summary>
/// Different modes for tracking systems
/// </summary>
public enum TrackingSystemMode
{
    Idle,
    Calibrating,
    Tracking
}

/// <summary>
/// Helper data class to provide additional information when changing between modes.
/// </summary>
public class TrackingSystemModeState
{
    /// <summary>
    /// Related mode.
    /// </summary>
    public TrackingSystemMode Mode { get; set; }
    /// <summary>
    /// Amount of values for this mode.
    /// </summary>
    public int AmountValues { get; set; }

    /// <summary>
    /// Standard instanciation - use { } to acces properties.
    /// </summary>
    public TrackingSystemModeState() { }

    /// <summary>
    /// Instanciates a new tracking system mode state with the given values.
    /// </summary>
    /// <param name="mode">Related mode</param>
    /// <param name="amountValues">Amount of value for this mode</param>
    public TrackingSystemModeState(TrackingSystemMode mode, int amountValues)
    {
        Mode = mode;
        AmountValues = amountValues;
    }

}
